<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <parent>
        <groupId>net.ihe.gazelle.simulators</groupId>
        <artifactId>simulator-common</artifactId>
        <version>5.0.0</version>
    </parent>

    <modelVersion>4.0.0</modelVersion>
    <groupId>net.ihe.gazelle.simulator.xdw</groupId>
    <artifactId>XDWSimulator</artifactId>
    <packaging>pom</packaging>
    <version>3.0.1-SNAPSHOT</version>
    <name>XDWSimulator</name>

    <scm>
        <connection>scm:git:${git.project.url}</connection>
        <developerConnection>scm:git:${git.project.url}</developerConnection>
        <url>${git.project.url}</url>
        <tag>HEAD</tag>
    </scm>

    <organization>
        <name>IHE</name>
        <url>http://www.ihe.net/</url>
    </organization>

    <ciManagement>
        <system>jenkins</system>
        <url>http://gazelle.ihe.net/jenkins/job/XDWSimulator/</url>
    </ciManagement>
    <developers />

    <contributors />

    <description>XDWSimulator tool (edition, validation, documentation)</description>
    <url>http://gazelle.ihe.net/XDWSimulator/</url>
    <issueManagement>
        <system>Jira</system>
        <url>http://gazelle.ihe.net/jira/browse/XDW</url>
    </issueManagement>

    <properties>
        <simulator.admin.name>Developper</simulator.admin.name>
        <simulator.admin.mail>developper@ihe.net</simulator.admin.mail>
        <mbv.doc>1.1.10</mbv.doc>
        <sonar.maven.plugin>3.5.0.1254</sonar.maven.plugin>
        <git.user.name>git</git.user.name>
        <git.user.token>git</git.user.token>
        <git.project.url>
            https://${git.user.name}:${git.user.token}@gitlab.inria.fr/gazelle/public/simulation/xdw-simulator
        </git.project.url>
        <nexus.url>https://gazelle.ihe.net/nexus</nexus.url>
    </properties>

    <dependencyManagement>
        <dependencies>

            <!-- Modules -->
            <dependency>
                <groupId>net.ihe.gazelle.simulator.xdw</groupId>
                <artifactId>XDWSimulator-ejb</artifactId>
                <version>3.0.1-SNAPSHOT</version>
                <type>ejb</type>
            </dependency>
            <dependency>
                <groupId>net.ihe.gazelle.simulator.xdw</groupId>
                <artifactId>XDWSimulator-war</artifactId>
                <version>3.0.1-SNAPSHOT</version>
                <type>war</type>
            </dependency>

            <dependency>
                <groupId>asm</groupId>
                <artifactId>asm</artifactId>
                <version>3.3.1</version>
                <scope>provided</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <!-- Manage plugin versions for build stability -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>${maven.release.plugin.version}</version>
                    <configuration>
                        <tagNameFormat>@{project.version}</tagNameFormat>
                        <autoVersionSubmodules>true</autoVersionSubmodules>
                        <releaseProfiles>release</releaseProfiles>
                    </configuration>
                </plugin>

            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.1</version>
                <configuration>
                    <source>1.7</source>
                    <target>1.7</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <skip>${skipUnitTests}</skip>
                    <argLine>${argLine} -Xmx1024m -XX:MaxPermSize=256m</argLine>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco.version}</version>
                <executions>
                    <execution>
                        <id>pre-unit-test</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>post-unit-test</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.sonarsource.scanner.maven</groupId>
                <artifactId>sonar-maven-plugin</artifactId>
                <version>${sonar.maven.plugin}</version>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>development</id>

            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>

            <properties>
                <!-- development mode (disable in production) -->
                <seam.debug>true</seam.debug>

                <!-- datasource configuration -->
                <jdbc.connection.url>jdbc:postgresql:xdw-simulator</jdbc.connection.url>
                <jdbc.driver.class>org.postgresql.Driver</jdbc.driver.class>
                <jdbc.user>gazelle</jdbc.user>
                <jdbc.password>gazelle</jdbc.password>
                <min.pool.size>1</min.pool.size>
                <max.pool.size>3</max.pool.size>

                <!-- package exploded war file -->
                <exploded.war.file>true</exploded.war.file>

                <!-- development mode (exclude in production) -->
                <exclude.bootstrap>false</exclude.bootstrap>

                <!-- persistence.xml configuration -->
                <hibernate.dialect>
                    org.hibernate.dialect.PostgreSQLDialect
                </hibernate.dialect>
                <hibernate.hbm2ddl.auto>
                    update
                </hibernate.hbm2ddl.auto>
                <hibernate.show_sql>
                    false
                </hibernate.show_sql>
            </properties>
        </profile>

        <profile>
            <id>production</id>
            <activation>
                <property>
                    <name>performRelease</name>
                    <value>true</value>
                </property>
            </activation>

            <properties>
                <!-- development mode (disable in production) -->
                <seam.debug>false</seam.debug>

                <!-- datasource configuration -->
                <jdbc.connection.url>jdbc:postgresql:xdw-simulator</jdbc.connection.url>
                <jdbc.driver.class>org.postgresql.Driver</jdbc.driver.class>
                <jdbc.user>gazelle</jdbc.user>
                <jdbc.password>gazelle</jdbc.password>
                <min.pool.size>1</min.pool.size>
                <max.pool.size>3</max.pool.size>

                <!-- development mode (exclude in production) -->
                <exclude.bootstrap>true</exclude.bootstrap>

                <!-- package exploded war file -->
                <exploded.war.file>false</exploded.war.file>

                <!-- persistence.xml configuration -->
                <hibernate.dialect>
                    org.hibernate.dialect.PostgreSQLDialect
                </hibernate.dialect>
                <hibernate.hbm2ddl.auto>
                    update
                </hibernate.hbm2ddl.auto>
                <hibernate.show_sql>
                    false
                </hibernate.show_sql>
            </properties>
        </profile>

        <profile>
            <id>release</id>
            <activation>
                <property>
                    <name>performRelease</name>
                    <value>true</value>
                </property>
            </activation>
        </profile>

    </profiles>

    <repositories>
        <repository>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
            <id>IHE</id>
            <name>IHE Public Maven Repository Group</name>
            <url>https://gazelle.ihe.net/nexus/content/groups/public/</url>
            <layout>default</layout>
        </repository>
    </repositories>

    <distributionManagement>
        <repository>
            <id>nexus-releases</id>
            <url>${nexus.url}/content/repositories/releases</url>
        </repository>
    </distributionManagement>

    <pluginRepositories>
        <pluginRepository>
            <id>ihe-services-plugins</id>
            <name>IHE Services Public Maven Repository Group</name>
            <url>https://gazelle.ihe.net/nexus/content/groups/public/</url>
            <layout>default</layout>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>

    <modules>
        <module>XDWSimulator-ejb</module>
        <module>XDWSimulator-war</module>
        <module>XDWSimulator-ear</module>
    </modules>
</project>
