DELETE FROM app_configuration WHERE variable='application_works_without_cas';
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'cas_enabled', 'true');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'ip_login', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'ip_login_admin', '.*');
