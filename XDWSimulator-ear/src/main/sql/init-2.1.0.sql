-- SQL statements which are executed at application startup if hibernate.hbm2ddl.auto is 'create' or 'create-drop'


INSERT INTO app_configuration(id, variable, value) VALUES (1, 'application_admin_email', 'eric.poiseau@inria.fr');
INSERT INTO app_configuration(id, variable, value) VALUES (2, 'application_issue_tracker_url', 'https://gazelle.ihe.net/jira/projects/XDW');
INSERT INTO app_configuration(id, variable, value) VALUES (3, 'application_name', 'XDWSimulator');
INSERT INTO app_configuration(id, variable, value) VALUES (4, 'application_release_notes_url', 'https://gazelle.ihe.net/jira/projects/XDW');
INSERT INTO app_configuration(id, variable, value) VALUES (5, 'application_time_zone', 'UTC+01');
INSERT INTO app_configuration(id, variable, value) VALUES (6, 'application_admin_name', 'Eric Poiseau');
INSERT INTO app_configuration (id, variable, value) VALUES (7, 'application_url', 'https://gazelle.ihe.net/XDWSimulator');
INSERT INTO app_configuration (id, variable, value) VALUES (8, 'xdw_xsd', '/opt/XDWSimulator/xsd/XDW-2015-08-18.xsd');
INSERT INTO app_configuration (id, variable, value) VALUES (9, 'application_works_without_cas', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (10, 'doc_path', '/opt/XDWSimulator/samples/');
INSERT INTO app_configuration (id, variable, value) VALUES (11, 'dot_path', 'dot');
INSERT INTO app_configuration (id, variable, value) VALUES (12, 'evs_url', 'https://gazelle.ihe.net/EVSClient');
INSERT INTO app_configuration (id, variable, value) VALUES (13, 'xdw_xslt_viewer', 'https://gazelle.ihe.net/xsl/XDW.xsl');


SELECT pg_catalog.setval('app_configuration_id_seq', 14, true);

INSERT INTO gs_test_instance_status(id, keyword, label_to_display, description) VALUES (1, 'started', 'started', 'started');
INSERT INTO gs_test_instance_status(id, keyword, label_to_display, description) VALUES (2, 'completed', 'completed', 'completed');
SELECT pg_catalog.setval('gs_test_instance_status_id_seq', 2, true);



