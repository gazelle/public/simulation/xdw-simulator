-- SQL statements which are executed at application startup if hibernate.hbm2ddl.auto is 'create' or 'create-drop'


INSERT INTO app_configuration(id, variable, value) VALUES (1, 'application_admin_email', 'eric.poiseau@inria.fr');
INSERT INTO app_configuration(id, variable, value) VALUES (2, 'application_issue_tracker_url', 'http://gazelle.ihe.net/jira');
INSERT INTO app_configuration(id, variable, value) VALUES (3, 'application_name', 'PDQ PDC Simulator');
INSERT INTO app_configuration(id, variable, value) VALUES (4, 'application_release_notes_url', 'http://gazelle.ihe.net');
INSERT INTO app_configuration(id, variable, value) VALUES (5, 'application_time_zone', 'UTC+01');
INSERT INTO app_configuration(id, variable, value) VALUES (6, 'application_admin_name', 'Eric Poiseau');

INSERT INTO app_configuration(id, variable, value) VALUES (7, 'xsl_location', 'http://gazelle.ihe.net/xsl/hl7Validation/resultStylesheet.xsl');
INSERT INTO app_configuration(id, variable, value) VALUES (8, 'application_url', 'http://localhost:8080/XDWSimulator');

INSERT INTO app_configuration(id, variable, value) VALUES (9, 'xdw_xsd', '/Users/aboufahj/Documents/eclipse-mdht/workspace/ITI/XDW/XDW-2011-09-13.xsd');
INSERT INTO app_configuration(id, variable, value) VALUES (10, 'xdw_xslt_viewer', 'http://gazelle.ihe.net/xsl/XDW.xsl');
INSERT INTO app_configuration(id, variable, value) VALUES (11, 'dot_path', '/Applications/Graphviz.app/Contents/MacOS/dot');
INSERT INTO app_configuration(id, variable, value) VALUES (12, 'doc_path', '/Users/aboufahj/tmp/');
INSERT INTO app_configuration (id, variable, value) VALUES (13, 'evs_url', 'https://gazelle.ihe.net/EVSClient');

SELECT pg_catalog.setval('app_configuration_id_seq', 14, true);

INSERT INTO gs_test_instance_status(id, keyword, label_to_display, description) VALUES (1, 'started', 'started', 'started');
INSERT INTO gs_test_instance_status(id, keyword, label_to_display, description) VALUES (2, 'completed', 'completed', 'completed');
SELECT pg_catalog.setval('gs_test_instance_status_id_seq', 2, true);



