package net.ihe.gazelle.xdw.validator.test.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import net.ihe.gazelle.voc.NullFlavor;
//import net.ihe.gazelle.xdw.editor.action.common.CommonEditorManager;

public class Util {
	
	/**
	 * Takes a file and copies its content into a string which is returned
	 * @param inFile
	 * @return
	 */
	public static String getFileContentToString(File inFile)
	{
		if (inFile == null)
			return null;
		
		FileReader fr;
		try {
			fr = new FileReader(inFile);
			StringBuffer stringToValidate = new StringBuffer();
			BufferedReader buff = new BufferedReader(fr);
			String line;
			while( (line = buff.readLine()) !=  null )
			{
				stringToValidate.append(line);
				stringToValidate.append("\n");
			}
			buff.close();
			fr.close();
			return stringToValidate.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}	
	}
	
	/**
	 * Creates a file from its path and calls the method which returns the string
	 * @param filePath
	 * @return
	 */
	public static String getFileContentToString(String filePath) {
		if (filePath == null || filePath.length() == 0)
			return null;
		File file = new File(filePath);
		if(file.exists())
			return getFileContentToString(file);
		else
			return null;
	}

	public static void main(String[] args) {
		//CommonEditorManager com = new CommonEditorManager();
		//String rr = com.getAttributeAsString(NullFlavor.ASKU, "net.ihe.gazelle.voc");
		if (NullFlavor.ASKU instanceof Enum) System.out.println(NullFlavor.ASKU);
		System.out.println("rr=" + NullFlavor.ASKU);
	}
	
}
