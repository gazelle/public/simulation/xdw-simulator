//package net.ihe.gazelle.xdw.validator.test;
//
//public class GraphvizTest {
//	private static final String DOT_EXE_PATH = "/Applications/Graphviz.app/Contents/MacOS/dot";
	
//	public static void main(String[] args) throws Exception {
//		Graph graph = new Graph();
//
//		// クラス図のスタイル
//		Style nodeStyle = new Style();
//		nodeStyle.attr(Attribute.FONTNAME, "Bitstream Vera Sans");
//		nodeStyle.attr(Attribute.FONTSIZE, 8f);
//		nodeStyle.attr("shape", "record");// Shape.RECORDが未定義なので文字列で書く
//
//		// 継承矢印のスタイル
//		Style edgeStyle = new Style();
//		edgeStyle.attr(Attribute.ARROWHEAD, Arrow.NONE);
//		edgeStyle.attr(Attribute.ARROWTAIL, Arrow.ONORMAL);
//
//		// 親クラス(Hoge)
//		Node hoge = new Node();
//		hoge.style(nodeStyle);
//		hoge.attr(Attribute.LABEL, "{Hoge|- fuga : string\\l- haga : int\\l|# piyo() : void\\l}");
//		graph.node(hoge);
//
//		// 子クラス(Foo)
//		Node foo = new Node();
//		foo.style(nodeStyle);
//		foo.attr(Attribute.LABEL, "{Foo|+ FOO : String|# piyo : void\\l}");
//		graph.node(foo);
//
//		// 子クラス(Bar)
//		Node bar = new Node();
//		bar.style(nodeStyle);
//		bar.attr(Attribute.LABEL, "{Bar|+ BAR : String|# piyo : void\\l}");
//		graph.node(bar);
//
//		// Foo->Hogeの矢印
//		Edge fooToHoge = new Edge(hoge, foo);
//		fooToHoge.style(edgeStyle);
//		graph.edge(fooToHoge);
//
//		// Bar->Hogeの矢印
//		Edge barToHoge = new Edge(hoge, bar);
//		barToHoge.style(edgeStyle);
//		graph.edge(barToHoge);
//
//		// PNG画像を出力
//		graph.generateTo(Arrays.asList(DOT_EXE_PATH, "-Tpng"), new File("test.png"));
//	}
//}
