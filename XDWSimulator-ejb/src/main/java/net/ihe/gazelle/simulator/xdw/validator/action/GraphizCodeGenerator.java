package net.ihe.gazelle.simulator.xdw.validator.action;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.oasis.TPart;
import net.ihe.gazelle.xdw.TXDWTask;
import net.ihe.gazelle.xdw.TXDWWorkflowDocument;
import net.ihe.gazelle.xdw.TXDWeventHistory;
import net.ihe.gazelle.xdw.TaskEventType;
import net.ihe.gazelle.xdw.TaskListType;
import net.ihe.gazelle.xdw.util.XDWTransformer;

import org.jboss.seam.util.Base64;
import org.kohsuke.graphviz.Arrow;
import org.kohsuke.graphviz.Attribute;
import org.kohsuke.graphviz.Edge;
import org.kohsuke.graphviz.Graph;
import org.kohsuke.graphviz.Node;
import org.kohsuke.graphviz.Style;

public class GraphizCodeGenerator {
	
	public static byte[] getXDotFromDocContent(String docContent, String dot, boolean isBase64){
		TXDWWorkflowDocument txdw;
		try {
			txdw = XDWTransformer.load(new ByteArrayInputStream(docContent.getBytes()));
		} catch (JAXBException e1) {
			e1.printStackTrace();
			return null;
		}
		Graph g = GraphizCodeGenerator.generateGraphForTest(txdw);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		List<String> commands = new ArrayList<String>();

		commands.add(dot);
		commands.add("-Tpng");


		try {
			g.generateTo(commands, bos);
			bos.close();
			if (isBase64){
				return Base64.encodeBytes(bos.toByteArray(), Base64.DONT_BREAK_LINES).getBytes();
			}
			else{
				return bos.toByteArray();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("restriction")
	public static Graph generateGraphForTest(TXDWWorkflowDocument txdw) {
		Graph g = new Graph();
		TaskListType taskl = txdw.getTaskList();
		
		Style nodeStyle = new Style();
		nodeStyle.attr(Attribute.FONTNAME, "Bitstream Vera Sans");
		nodeStyle.attr(Attribute.FONTSIZE, 12f);
		nodeStyle.attr("shape", "record");
		
		
		Style edgeStyle = new Style();
		edgeStyle.attr(Attribute.ARROWHEAD, Arrow.ONORMAL);
		edgeStyle.attr(Attribute.ARROWTAIL, Arrow.NONE);
		
		List<Node> lntask = new ArrayList<Node>();
		
		Map<String, Node> tasksNodes = new HashMap<String, Node>();
		if (taskl != null){
			List<TXDWTask> LXDWTask = txdw.getTaskList().getXDWTask();
			if (LXDWTask != null){
				Node source = null;
				for (TXDWTask txdwTask : LXDWTask) {
					String taskLabel = txdwTask.getTaskData().getTaskDetails().getName() + "-" + txdwTask.getTaskData().getTaskDetails().getId();
					Node taskN = tasksNodes.get(taskLabel);
					if (taskN == null){
						taskN = new Node();
						taskN.style(nodeStyle);
						String label = "{ Task :" + taskLabel + "|- Task Type:  " + txdwTask.getTaskData().getTaskDetails().getTaskType() + 
						"\\l- Status: " + txdwTask.getTaskData().getTaskDetails().getStatus() + 
						"\\l- Created Time: " + txdwTask.getTaskData().getTaskDetails().getCreatedTime() + 
						"\\l- Last Modified Time: " + txdwTask.getTaskData().getTaskDetails().getLastModifiedTime();
						for (TPart part : txdwTask.getTaskData().getInput().getPart()) {
							label += "\\l- Input: " + part.getName();
						}
						for (TPart part : txdwTask.getTaskData().getOutput().getPart()) {
							label += "\\l- Output: " + part.getName();
						}
						label += "\\l}";
						taskN.attr(Attribute.LABEL, label);
						taskN.attr("rankdir", "LR");
						g.node(taskN);
						tasksNodes.put(taskLabel, taskN);
//						if (source != null){
//							Edge edge = new Edge(source, taskN);
//							edge.style(edgeStyle);
//							g.edge(edge);
//						}
						TXDWeventHistory lte = txdwTask.getTaskEventHistory();
						if (lte != null){
							Node sourceEv = null;
							List<TaskEventType> ltev = lte.getTaskEvent();
							for (TaskEventType taskEventType : ltev) {
								Node even = new Node();
								even.style(nodeStyle);
								String labelEv = "{" + taskEventType.getId() + "|- eventTime: " + taskEventType.getEventTime() + 
								"\\l- identifier: " + taskEventType.getIdentifier() + 
								"\\l- eventType: " + taskEventType.getEventType() + 
								"\\l- Status: " + taskEventType.getStatus();
								labelEv +=  "\\l}";
								even.attr(Attribute.LABEL, labelEv);
								g.node(even);
								if (sourceEv != null){
									Edge edge = new Edge(sourceEv, even);
									edge.style(edgeStyle);
									g.edge(edge);
								}
								else {
									Edge edge = new Edge(taskN, even);
									edge.style(edgeStyle);
									g.edge(edge);
								}
								sourceEv = even;
							}
						}
						
						source = taskN;
						lntask.add(taskN);
					}
				}
			}
		}
		
		g.attr("overlap", "false");
		//g.attr("rankdir", "TB");
		
		Graph sub = g.attr("rank", "same");

		for (Node nd : lntask) {
			sub.node(nd);
		}

		return g;
	}
	
	public static void main(String[] args) {
		/*TXDWWorkflowDocument txdw = XDWTransformer.load(new FileInputStream(new File("/Users/aboufahj/Documents/XDW-workspace/net.ihe.gazelle.xdw.code/samples/example.xml")));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		String dot = "/Applications/Graphviz.app/Contents/MacOS/dot";
		XDWTransformer.save(baos, txdw);
		String rr = new String(GraphizCodeGenerator.getXDotFromDocContent(baos.toString(), dot, true));
		System.out.println("rr=" + rr);*/
	}

}
