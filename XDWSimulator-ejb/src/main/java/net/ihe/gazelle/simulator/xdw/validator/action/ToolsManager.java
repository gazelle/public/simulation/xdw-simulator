package net.ihe.gazelle.simulator.xdw.validator.action;

import java.io.ByteArrayInputStream;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.xmltools.xsl.XSLTransformer;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

@Name("XDWToolsMan")
@Scope(ScopeType.PAGE)
public class ToolsManager {
	
	@Logger
	private static Log log;
	
	private String xdwDoc;
	
	private byte[] xdwDiagram;
	
	private String xslContent;
	
	private String uploadOrEdit = "Upload";
	
	public String getUploadOrEdit() {
		return uploadOrEdit;
	}

	public void setUploadOrEdit(String uploadOrEdit) {
		this.uploadOrEdit = uploadOrEdit;
	}

	public String getXslContent() {
		return xslContent;
	}

	public void setXslContent(String xslContent) {
		this.xslContent = xslContent;
	}

	public byte[] getXdwDiagram() {
		return xdwDiagram;
	}

	public void setXdwDiagram(byte[] xdwDiagram) {
		this.xdwDiagram = xdwDiagram;
	}

	public String getXdwDoc() {
		return xdwDoc;
	}

	public void setXdwDoc(String xdwDoc) {
		this.xdwDoc = xdwDoc;
	}

	public String getXDotSession(){
		return (String) Contexts.getSessionContext().get("xDot");
	}

	public void generateXDot(){
		if (this.xdwDoc != null && !this.xdwDoc.equals("")){
			String dot = ApplicationConfiguration.getValueOfVariable("dot_path");
			this.xdwDiagram = GraphizCodeGenerator.getXDotFromDocContent(xdwDoc, dot, false);
			this.xslContent = this.getContentAsHtml();
		}
	}
	
	private String getContentAsHtml(){
		if (xdwDoc != null && !xdwDoc.equals("")){
			String inXslPath = ApplicationConfiguration.getValueOfVariable("xdw_xslt_viewer");
			String res = XSLTransformer.resultTransformation(new ByteArrayInputStream(xdwDoc.getBytes()), inXslPath, null);
			return res;
		}
		return null;
	}
	
	public void uploadEventlistener(FileUploadEvent event) {
		try {
			UploadedFile item = event.getUploadedFile();
			if (item.getData() != null && item.getData().length > 0) {
				this.xdwDoc = new String(item.getData());
			} else {
				FacesMessages.instance().add("File is empty.");
			}
		} catch (Exception e) {
			log.error("uploadEventListener: an error occurred - " + e.getMessage());
			return;
		}
	}

	public String getDocumentationRoot(){
		return ApplicationConfiguration.getValueOfVariable("doc_root");
	}
	

}
