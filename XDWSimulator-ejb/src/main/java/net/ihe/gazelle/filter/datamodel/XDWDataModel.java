package net.ihe.gazelle.filter.datamodel;

import java.util.Date;

import javax.persistence.EntityManager;

import net.ihe.gazelle.ssov7.gum.client.interlay.filter.UserValueFormatter;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.xdw.model.XDWDocument;
import net.ihe.gazelle.xdw.model.XDWDocumentAttributes;
import net.ihe.gazelle.xdw.model.XDWDocumentQuery;

public class XDWDataModel extends FilterDataModel<XDWDocument> {

	private Date endDate;
	
	private Date startDate;
	
	private String author;
	
	private String path;

	public String getAuthor() {
		return author;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public XDWDataModel() {
		super(new Filter<XDWDocument>(getCriterionList()));
		super.getFilter().getFormatters().put("author", new UserValueFormatter(super.getFilter(), "author"));
	}
	
	private static HQLCriterionsForFilter<XDWDocument> getCriterionList() {
		XDWDocumentQuery query = new XDWDocumentQuery();
		HQLCriterionsForFilter<XDWDocument> result = query.getHQLCriterionsForFilter();
		addTestCriterions(result, query, true);
		return result;
	}
	
	public static void addTestCriterions(HQLCriterionsForFilter<XDWDocument> result, 
			XDWDocumentAttributes<XDWDocument> messAttributes,
			boolean onlyConnectathon) {
		result.addPath("author", messAttributes.author());
		result.addPath("transaction", messAttributes.path());
	}
	
	public void clear() {
		getFilter().clear();
		author = null;
		path = null;
		startDate = null;
		endDate = null;
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<XDWDocument> queryBuilder) {
		super.appendFiltersFields(queryBuilder);
		 if (startDate != null){
		      queryBuilder.addRestriction(HQLRestrictions.ge("creationDate", startDate));
		  }
		  
		  if (endDate != null){
		      queryBuilder.addRestriction(HQLRestrictions.le("creationDate", endDate));
		  }
		  
	}

	@Override
	protected Object getId(XDWDocument t) {
		// TODO Auto-generated method stub
		return t.getId();
	}
	
	

}
