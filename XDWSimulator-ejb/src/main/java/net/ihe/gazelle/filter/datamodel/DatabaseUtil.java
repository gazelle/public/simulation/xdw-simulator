package net.ihe.gazelle.filter.datamodel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.model.SelectItem;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.jboss.seam.core.ResourceBundle;

public class DatabaseUtil {
	
	public enum RestrictionType{
		ILIKE,
		IN,
		EQ,
		GE,
		LE,
		LIKE,
		BETWEEN;
	}

	public enum When{
		after,
		on,
		before,
	}
	
	
	public static List<SelectItem> getWhenValuesAsSelectItems()
	{
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(When.before, ResourceBundle.instance().getString("gazelle.evs.client.before")));
		items.add(new SelectItem(When.on, ResourceBundle.instance().getString("gazelle.evs.client.on")));
		items.add(new SelectItem(When.after, ResourceBundle.instance().getString("gazelle.evs.client.after")));
		return items;
	}
	/**
	 * 
	 * @param criterion
	 * @param type
	 * @param attribute
	 * @param value1
	 * @param value2 TODO
	 * @return
	 */
	public static Criterion addCriterion(Criterion criterion, RestrictionType type, String attribute, Object value1, Object value2)
	{
		if (type != null)
		{
			Criterion newCriterion = null;
			switch (type) {
			case ILIKE:
				newCriterion = Restrictions.ilike(attribute, value1);
				break;
			case EQ:
				newCriterion = Restrictions.eq(attribute, value1);
				break;
			case GE:
				newCriterion = Restrictions.ge(attribute, value1);
				break;
			case LE:
				newCriterion = Restrictions.le(attribute, value1);
				break;
			case LIKE:
				newCriterion = Restrictions.like(attribute, value1);
				break;
			case BETWEEN:
				newCriterion = Restrictions.between(attribute, value1, value2);
				break;
			default:
				break;
			}
			if (newCriterion != null)
			{
				if (criterion == null)
					return newCriterion;
				else
					return Restrictions.and(criterion, newCriterion);
			}
			else
				return null;
		}
		else return null;
	}
	
	/**
	 * 
	 * @param queryString
	 * @return
	 */
	public static StringBuffer addANDorWHERE(StringBuffer queryString)
	{
		if (queryString.length() > 0)
			return queryString.append(" AND ");
		else
			return queryString.append(" WHERE ");
	}
	
	/**
	 * When the user select "on date" as search criteria, the restriction on date must be between 'day at midnight' and 'day + 1 at midnight'
	 *
	 * @param inDate : date selected in rich:calendar component
	 * @return a map of dates (with keys "begin" and "end") which correspond to this two dates 
	 */
	public static HashMap<String, Date> getBeginAndEnd(Date inDate)
	{
		if (inDate == null)
			return null;
		
		HashMap<String, Date> dates = new HashMap<String, Date>();
		Calendar date = Calendar.getInstance();
		date.setTime(inDate);
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		dates.put("begin", date.getTime());
		date.add(Calendar.DAY_OF_MONTH, +1);
		dates.put("end", date.getTime());
		return dates;
	}
	
}
