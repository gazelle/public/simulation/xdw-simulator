package net.ihe.gazelle.xdw.validator;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.validation.Notification;

public class ValidatorException extends JAXBException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 private List<Notification> diagnostic;

	public List<Notification> getDiagnostic() {
		if (diagnostic == null){
			diagnostic = new ArrayList<Notification>();
		}
		return diagnostic;
	}

	public void setDiagnostic(List<Notification> diagnostic) {
		this.diagnostic = diagnostic;
	}

	public ValidatorException(String message, List<Notification> diagnostic) {
		super(message);
		
	}

}
