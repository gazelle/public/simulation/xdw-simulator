package net.ihe.gazelle.xdw.converter;
import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import net.ihe.gazelle.xdw.model.XDWDocument;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.log.Log;

@BypassInterceptors
@Name("xdwDocumentConverter")
@Converter(forClass=XDWDocument.class)
public class XDWDocumentConverter implements javax.faces.convert.Converter, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6167087068631260012L;
	
	@Logger
	private static Log log;

	public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
		if(value != null)
		{
			try{
				Integer id = Integer.parseInt(value);
				if (id != null)
				{
					EntityManager em = (EntityManager) Component.getInstance("entityManager");
					XDWDocument ot = em.find(XDWDocument.class, id);
					return ot;
				}
			}catch(NumberFormatException e){
				log.error(e.getMessage());
			}
		}
		return null;
	}

	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
		if(value instanceof XDWDocument)
		{
			XDWDocument ot = (XDWDocument) value;
			return ot.getId().toString();
		}
		else
			return null;
	}
	

}
