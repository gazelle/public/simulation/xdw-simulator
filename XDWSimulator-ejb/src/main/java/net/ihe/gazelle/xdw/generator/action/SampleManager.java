package net.ihe.gazelle.xdw.generator.action;

import net.ihe.gazelle.edit.Transform;
import net.ihe.gazelle.filter.datamodel.XDWDataModel;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.xdw.TXDWWorkflowDocument;
import net.ihe.gazelle.xdw.editor.action.xdw.XDWTXDWWorkflowDocumentEditorLocal;
import net.ihe.gazelle.xdw.model.XDWDocument;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;
@Name("sampleManager")
@Scope(ScopeType.PAGE)
public class SampleManager implements UserAttributeCommon {
	
	@Logger
	private static Log log;
	private transient XDWDataModel xdws;
	
	private XDWDocument selectedXDWDocument;
	
	private Date startDate;
	
	private Date endDate;

	@In(value="gumUserService")
	private UserService userService;


	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.getXdws().setStartDate(startDate);
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.getXdws().setEndDate(endDate);
		this.endDate = endDate;
	}

	/**
	 * Used in samples.xhtml and xdw.xhtml
	 * @param userId The user id
	 * @return The name of the user
	 */
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}

	public XDWDocument getSelectedXDWDocument() {
		return selectedXDWDocument;
	}

	public void setSelectedXDWDocument(XDWDocument selectedXDWDocument) {
		this.selectedXDWDocument = selectedXDWDocument;
	}
	
	public void initXDWDoc(){
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		EntityManager em = (EntityManager)org.jboss.seam.Component.getInstance("entityManager");
        String pageid = params.get("id");
        if (pageid != null){
        	if (pageid != ""){
        		try{
                    int id = Integer.valueOf(pageid);
                    this.selectedXDWDocument = em.find(XDWDocument.class, id);
            }
            catch(NumberFormatException e){
                    log.info("The Id has not a good format");
                    FacesMessages.instance().add("#{messages['gazelle.testmanagement.object.ProblemIdFormat']}") ;
            }
        	}
        }
	}
	

	


	public XDWDataModel getXdws() {
		if (xdws == null) xdws = new XDWDataModel();
		return xdws;
	}

	public XDWDataModel getSandaloneXdws() {
		return xdws;
	}

	public void setXdws(XDWDataModel xdws) {
		this.xdws = xdws;
	}
	
	public List<SelectItem> getWhenValuesAsSelectItems() {
		return net.ihe.gazelle.filter.datamodel.DatabaseUtil.getWhenValuesAsSelectItems();
	}
	
	public String permanentLink(XDWDocument xdw){
		if (xdw != null){
			return ApplicationConfiguration.getValueOfVariable("application_url") + "/xdw.seam?id=" + xdw.getId();
		}
		return null;
	}
	
	public String editXDWDocument(XDWDocument xdw){
		if (xdw != null){
			String content  = xdw.getContent();
			try {
				TXDWWorkflowDocument xx = Transform.load(new ByteArrayInputStream(content.getBytes()));
				XDWTXDWWorkflowDocumentEditorLocal ll = (XDWTXDWWorkflowDocumentEditorLocal)Component.getInstance("xDWTXDWWorkflowDocumentEditorManager");
				ll.setSelectedXDWTXDWWorkflowDocument(xx);
			} catch (JAXBException e) {
				FacesMessages.instance().add("The document is not well formed. The edition is impossible to do.");
				return null;
			}
			return "/editor/xdw/TXDWWorkflowDocument.xhtml";
		}
		return null;
	}
	
	public boolean canRender(XDWDocument xdw){
		if (Identity.instance().getCredentials().getUsername() != null){
			if (Identity.instance().getCredentials().getUsername().equals(xdw.getAuthor())){
				return true;
			}
		}
		return false;
	}

	public void applyFilter(){
        this.getXdws().resetCache();
    }
	
	public void resetFilter(){
		this.setEndDate(null);
		this.setStartDate(null);
		this.getXdws().getFilter().clear();
		this.getXdws().resetCache();
	}


	


	
}
