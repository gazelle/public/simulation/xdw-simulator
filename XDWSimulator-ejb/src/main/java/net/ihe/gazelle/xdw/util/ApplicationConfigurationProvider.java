package net.ihe.gazelle.xdw.util;

import java.io.Serializable;

import javax.ejb.Remove;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;

@Name("applicationConfigurationProvider")
@Scope(ScopeType.APPLICATION)
@GenerateInterface("ApplicationConfigurationProviderLocal")
public class ApplicationConfigurationProvider implements Serializable, ApplicationConfigurationProviderLocal {

    /**
     *
     */
    private static final long serialVersionUID = 5004043953931007769L;

    @Destroy
    @Remove
    public void destroy() {
    }


    public String loginByIP() {
        Identity identity = (Identity) Component.getInstance("org.jboss.seam.security.identity");
        if ("loggedIn".equals(identity.login())) {
            return "/home.xhtml";
        } else {
            return null;
        }
    }

}
