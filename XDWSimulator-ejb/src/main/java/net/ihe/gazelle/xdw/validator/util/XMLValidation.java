package net.ihe.gazelle.xdw.validator.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import net.ihe.gazelle.edit.FileReadWrite;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.xmltools.xsd.ValidationException;
import net.ihe.gazelle.xmltools.xsd.XSDValidator;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;

public class XMLValidation {
	
	private static SAXParserFactory factoryBASIC;
	
	private static SAXParserFactory docfactory;

	static{
		try {
			factoryBASIC = SAXParserFactory.newInstance();
			factoryBASIC.setValidating(false);
			factoryBASIC.setNamespaceAware(true);
		} catch (Exception e){}
		
		try {
			docfactory = SAXParserFactory.newInstance();
			docfactory.setNamespaceAware( true);
			SchemaFactory sfactory = 
					SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
			Schema schema = sfactory.newSchema(new File(ApplicationConfiguration.getValueOfVariable("xdw_xsd")));
			docfactory.setSchema(schema);
		} catch (Exception e){}
	}
	
	/**
	 * Parses the file using SAX to check that it is a well-formed XML file
	 * @param file
	 * @return
	 */
	public static DocumentWellFormed isXMLWellFormed(String string){
		return validateIfDocumentWellFormedXML(string, "", factoryBASIC);
	}
	
	private static DocumentWellFormed validateIfDocumentWellFormedXML(String cdaDocument, String xsdpath, SAXParserFactory factory){
		DocumentWellFormed dv = new DocumentWellFormed();
		return validXMLUsingXSD(cdaDocument, xsdpath, factory, dv);
	}
	
	private static <T extends DocumentValidXSD> T validXMLUsingXSD(String cdaDocument, String xsdpath, SAXParserFactory factory, T dv)
	{
		List<ValidationException> exceptions = new ArrayList<ValidationException>();
		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(cdaDocument.getBytes("UTF8"));
			XSDValidator xsdValidator = new XSDValidator();
			exceptions = xsdValidator.validateUsingFactoryAndSchema(bais, xsdpath,  factory);
		} catch(Exception e){
			exceptions.add(handleException(e));
		}
		return extractValidationResult(exceptions, xsdpath, factory, dv);
	}

	
	
	/**
	 * Checks that the XDW document is valid (uses XDW.xsd file)
	 * @param file
	 * @return
	 */
	public static DocumentValidXSD isXDWValid(String xdwDocument)
	{
		if (xdwDocument == null){
			return null;
		}
		else {
			return validXMLUsingXSD(xdwDocument);
		}
	}
	
	private static DocumentValidXSD validXMLUsingXSD(String xdwDocument)
	{
		return validXMLUsingXSD(xdwDocument, ApplicationConfiguration.getValueOfVariable("xdw_xsd"), docfactory);
	}
	
	private static DocumentValidXSD validXMLUsingXSD(String cdaDocument, String xsdpath, SAXParserFactory factory)
	{
		DocumentValidXSD dv = new DocumentValidXSD();
		return validXMLUsingXSD(cdaDocument, "", factory, dv);
	}
	
	private static <T extends DocumentValidXSD> T extractValidationResult(List<ValidationException> exceptions, String xsdpath, SAXParserFactory factory, T dv)
	{

		dv.setResult("PASSED");

		if (exceptions == null || exceptions.size() == 0)
		{	
			dv.setResult("PASSED");
			return dv;
		}
		else
		{
			Integer nbOfErrors = 0;
			Integer nbOfWarnings = 0;
			Integer exceptionCounter = 0;
			for (ValidationException ve : exceptions)
			{
				if (ve.getSeverity() == null){
					ve.setSeverity("error");
				}
				exceptionCounter ++;
				if ((ve.getSeverity() != null) && (ve.getSeverity().equals("warning"))){
					nbOfWarnings ++;
				}
				else {
					nbOfErrors ++;
				}
				XSDMessage xsd = new XSDMessage();
				xsd.setMessage(ve.getMessage());
				xsd.setSeverity(ve.getSeverity());
				if (StringUtils.isNumeric(ve.getLineNumber())){
					xsd.setLineNumber(Integer.valueOf(ve.getLineNumber()));
				}
				if (StringUtils.isNumeric(ve.getColumnNumber())){
					xsd.setColumnNumber(Integer.valueOf(ve.getColumnNumber()));
				}
				dv.getXSDMessage().add(xsd);
			}
			dv.setNbOfErrors(nbOfErrors.toString());
			dv.setNbOfWarnings(nbOfWarnings.toString());
			if (nbOfErrors > 0){
				dv.setResult("FAILED");
			}
			return dv;
		}

	}
	
	private static ValidationException handleException(Exception e)
	{
		ValidationException ve = new ValidationException();
		ve.setLineNumber("0");
		ve.setColumnNumber("0");
		if (e != null && e.getMessage() != null){
			ve.setMessage("error on validating : " + e.getMessage());
		} 
		else if (e != null && e.getCause() != null && e.getCause().getMessage() != null){
			ve.setMessage("error on validating : " + e.getCause().getMessage());
		}
		else{
			ve.setMessage("error on validating. The exception generated is of kind : " + e.getClass().getSimpleName());
		}
		ve.setSeverity("error");
		return ve;
	}
	
	public static void main(String[] args) {
		try {
			String doc = FileReadWrite.readDoc("/Users/aboufahj/Documents/workspace/XDWSimulator/XDWSimulator-ejb/src/test/resources/faltcase/test.xml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
