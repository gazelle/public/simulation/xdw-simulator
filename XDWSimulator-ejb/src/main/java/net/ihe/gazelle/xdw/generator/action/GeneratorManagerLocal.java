package net.ihe.gazelle.xdw.generator.action;

import javax.ejb.Local;

@Local
public interface GeneratorManagerLocal {

	public String generateXDWWorkflowDocument();
	public String generateXDWWorkflowDocument(String xdwDoc);
	
	public void downloadFile(String string);
	public void validate(String comment);
	
	public String getFileToSaveName();
	public void setFileToSaveName(String fileToSaveName);
	public void initSaveAction();
	public void saveXDWDoc(String doc);
	
	public void destroy();
	
}