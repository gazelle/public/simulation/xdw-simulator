package net.ihe.gazelle.xdw.validator;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.soap.SOAPException;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.xdw.validator.action.GraphizCodeGenerator;

import org.jboss.seam.annotations.JndiName;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.util.Base64;


@Stateless
@Name("XDWViewer")
@WebService(targetNamespace = "urn:ihe:iti:2011:xdw", name = "Viewer_PortType")
//@JndiName("java:app/XDWSimulator-ejb/XDWViewer")
@SOAPBinding(style = Style.DOCUMENT, parameterStyle = ParameterStyle.BARE)

public class XDWViewerWS implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

    @WebMethod(operationName = "getPngDiagram")
    @WebResult(name = "PngDiagram")
	public byte[] getPngDiagram(@WebParam(name="base64File")String base64File) throws SOAPException{
		byte[] res = null;
		byte[] bb = Base64.decode(base64File);
		String xdwDocument = new String(bb);
		String dot = ApplicationConfiguration.getValueOfVariable("dot_path");
		res = GraphizCodeGenerator.getXDotFromDocContent(xdwDocument,dot, false);
		return res;
	}
	
}


