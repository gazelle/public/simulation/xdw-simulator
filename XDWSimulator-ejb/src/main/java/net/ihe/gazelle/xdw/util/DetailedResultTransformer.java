package net.ihe.gazelle.xdw.util;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.validation.DetailedResult;

public class DetailedResultTransformer {
	
	public static DetailedResult load(InputStream is) throws JAXBException{
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.validation");
		Unmarshaller u = jc.createUnmarshaller();
		DetailedResult res = (DetailedResult) u.unmarshal(is);
		return res;
	}
	
	public static void save(OutputStream os, DetailedResult txdw) throws JAXBException{
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.validation");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF8");
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.marshal(txdw, os);
	}

}
