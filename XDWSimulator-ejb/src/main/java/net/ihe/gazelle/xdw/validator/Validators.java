package net.ihe.gazelle.xdw.validator;

public enum Validators {
	
	XDW_BASIC("XDW - BASIC"), XBER_WD("XBeR - Workflow Definition"), 
	XTHM_WD("XTHM - Workflow Definition"), CMPD1_WD("CMPD-WD scenario 1"),
	CMPD2_WD("CMPD-WD scenario 2");
	
	private Validators(String value) {
		this.value = value;
	}
	
	String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public static Validators getValidatorFromValue(String value){
		for (Validators val : Validators.values()) {
			if (val.value.equals(value)) return val;
		}
		return null;
	}

}
