package net.ihe.gazelle.xdw.validator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.cda.CDAValidator;
import net.ihe.gazelle.datatypes.datatypes.DATATYPESPackValidator;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.oasis.oasis.OASISPackValidator;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.ValidationResultsOverview;
import net.ihe.gazelle.xdw.TXDWWorkflowDocument;
import net.ihe.gazelle.xdw.util.DetailedResultTransformer;
import net.ihe.gazelle.xdw.util.XDWTransformer;
import net.ihe.gazelle.xdw.validator.util.XMLValidation;
import net.ihe.gazelle.xdw.wd.cmpd.CMPDPackValidator;
import net.ihe.gazelle.xdw.wd.cmpd1.CMPD1PackValidator;
import net.ihe.gazelle.xdw.wd.cmpd2.CMPD2PackValidator;
import net.ihe.gazelle.xdw.wd.xber.XBERPackValidator;
import net.ihe.gazelle.xdw.wd.xthm.XTHMPackValidator;
import net.ihe.gazelle.xdw.xdw.XDWPackValidator;

public class MicroValidation {
	
	private static String validateXDWByValidators(String document, List<ConstraintValidatorModule> validators, Validators val){
		DetailedResult dr = new DetailedResult();

		DocumentWellFormed dd = XMLValidation.isXMLWellFormed(document);
		dr.setDocumentWellFormed(dd);

		DocumentValidXSD ss = XMLValidation.isXDWValid(document);
		dr.setDocumentValidXSD(ss);

		TXDWWorkflowDocument txdw = null;
		List<Notification> ln = new ArrayList<Notification>();
		try {
			txdw = XDWTransformer.load(new ByteArrayInputStream(document.getBytes()));
			for (ConstraintValidatorModule constraintValidatorModule : validators) {
				TXDWWorkflowDocument.validateByModule(txdw, "/XDW.WorkflowDocument", constraintValidatorModule, ln);
			}
		}catch (ValidatorException e) {
			errorWhenExtracting(val, e, ln);
		}
		catch (Exception e) {
			errorWhenExtracting(val, ln);
		}
		dr.setMDAValidation(new MDAValidation());
		dr.getMDAValidation().setResult("PASSED");
		for (Notification notification : ln) {
			dr.getMDAValidation().getWarningOrErrorOrNote().add(notification);
			if (notification instanceof Error){
				dr.getMDAValidation().setResult("FAILED");
			}
		}
		summarizeDetailedResult(dr);
		String res = getDetailedResultAsString(dr);
		if (res.contains("?>")){
			res = res.substring(res.indexOf("?>") + 2);
		}
		return res;
	}
	
	static void errorWhenExtracting(Validators val, ValidatorException vexp, List<Notification> ln){
		if (ln ==null) return;
		if (vexp != null){
			if (vexp.getDiagnostic() != null){
				for (Notification notification : vexp.getDiagnostic()) {
					ln.add(notification);
				}
			}
		}
	}
	
	static void errorWhenExtracting(Validators val, List<Notification> ln){
		if (ln ==null) return;
		Error err = new Error();
		err.setTest("structure");
		err.setLocation("All the document");
		err.setDescription("The tool is not able to find urn:hl7-org:v3:ClinicalDocument element as the root of the validated document.");
		ln.add(err);
	}

	public static String validateXDW_BASIC(String document){
		return MicroValidation.validateXDWByValidators(document, MicroValidation.validatorsBasicXDWDocument(), Validators.XDW_BASIC);
	}
	
	public static String validateXBER_WD(String document){
		return MicroValidation.validateXDWByValidators(document, MicroValidation.validatorsXBERDocument(), Validators.XBER_WD);
	}
	
	public static String validateXTHM_WD(String document){
		return MicroValidation.validateXDWByValidators(document, MicroValidation.validatorsXTHMDocument(), Validators.XTHM_WD);
	}
	
	public static String validateCMPD1_WD(String document){
		return MicroValidation.validateXDWByValidators(document, MicroValidation.validatorsCMPD1Document(), Validators.CMPD1_WD);
	}
	
	public static String validateCMPD2_WD(String document){
		return MicroValidation.validateXDWByValidators(document, MicroValidation.validatorsCMPD2Document(), Validators.CMPD2_WD);
	}
	
	private static void summarizeDetailedResult(DetailedResult dr){
		if (dr != null){
			Date dd = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy, MM dd");
			DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
			dr.setValidationResultsOverview(new ValidationResultsOverview());
			dr.getValidationResultsOverview().setValidationDate(dateFormat.format(dd));
			dr.getValidationResultsOverview().setValidationTime(timeFormat.format(dd));
			dr.getValidationResultsOverview().setValidationServiceName("Gazelle XDW Validation");
			dr.getValidationResultsOverview().setValidationTestResult("PASSED");
			if ((dr.getDocumentValidXSD() != null) && (dr.getDocumentValidXSD().getResult() != null) && (dr.getDocumentValidXSD().getResult().equals("FAILED"))){
				dr.getValidationResultsOverview().setValidationTestResult("FAILED");
			}
			if ((dr.getDocumentWellFormed() != null) && (dr.getDocumentWellFormed().getResult() !=null) && (dr.getDocumentWellFormed().getResult().equals("FAILED"))){
				dr.getValidationResultsOverview().setValidationTestResult("FAILED");
			}
			if ((dr.getMDAValidation() != null) && (dr.getMDAValidation().getResult() != null) && (dr.getMDAValidation().getResult().equals("FAILED"))){
				dr.getValidationResultsOverview().setValidationTestResult("FAILED");
			}
		}
	}
	
	private static String getDetailedResultAsString(DetailedResult dr){
		if (dr != null){
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				DetailedResultTransformer.save(baos, dr);
			} catch (JAXBException e) {
				e.printStackTrace();
			}
			String res = baos.toString();
			return res!=null?res:"";
		}
		return "";
	}
	
	private static List<ConstraintValidatorModule> validatorsBasicXDWDocument(){
		List<ConstraintValidatorModule> diagnostic = new ArrayList<ConstraintValidatorModule>();
		diagnostic.add( new XDWPackValidator());
		diagnostic.add(new CDAValidator());
		diagnostic.add(new DATATYPESPackValidator());
		diagnostic.add(new OASISPackValidator());
		return diagnostic;
	}
	
	private static List<ConstraintValidatorModule> validatorsXBERDocument(){
		List<ConstraintValidatorModule> diagnostic = new ArrayList<ConstraintValidatorModule>();
		diagnostic.add( new XDWPackValidator());
		diagnostic.add(new CDAValidator());
		diagnostic.add(new DATATYPESPackValidator());
		diagnostic.add(new OASISPackValidator());
		diagnostic.add(new XBERPackValidator());
		return diagnostic;
	}
	
	private static List<ConstraintValidatorModule> validatorsXTHMDocument(){
		List<ConstraintValidatorModule> diagnostic = new ArrayList<ConstraintValidatorModule>();
		diagnostic.add( new XDWPackValidator());
		diagnostic.add(new CDAValidator());
		diagnostic.add(new DATATYPESPackValidator());
		diagnostic.add(new OASISPackValidator());
		diagnostic.add(new XTHMPackValidator());
		return diagnostic;
	}
	
	private static List<ConstraintValidatorModule> validatorsCMPD1Document(){
		List<ConstraintValidatorModule> diagnostic = new ArrayList<ConstraintValidatorModule>();
		diagnostic.add( new XDWPackValidator());
		diagnostic.add(new CDAValidator());
		diagnostic.add(new DATATYPESPackValidator());
		diagnostic.add(new OASISPackValidator());
		diagnostic.add(new CMPDPackValidator());
		diagnostic.add(new CMPD1PackValidator());
		return diagnostic;
	}
	
	private static List<ConstraintValidatorModule> validatorsCMPD2Document(){
		List<ConstraintValidatorModule> diagnostic = new ArrayList<ConstraintValidatorModule>();
		diagnostic.add( new XDWPackValidator());
		diagnostic.add(new CDAValidator());
		diagnostic.add(new DATATYPESPackValidator());
		diagnostic.add(new OASISPackValidator());
		diagnostic.add(new CMPDPackValidator());
		diagnostic.add(new CMPD2PackValidator());
		return diagnostic;
	}

}
