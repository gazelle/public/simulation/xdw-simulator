package net.ihe.gazelle.xdw.validator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.soap.SOAPException;

import net.ihe.gazelle.validation.exception.GazelleValidationException;
import net.ihe.gazelle.validation.model.ValidatorDescription;
import net.ihe.gazelle.validation.ws.AbstractModelBasedValidation;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.jboss.seam.util.Base64;


@Stateless
@Name("ModelBasedValidationWS")
@WebService(name = "ModelBasedValidationWS", 
		serviceName = "ModelBasedValidationWSService", 
		portName = "ModelBasedValidationWSPort", 
		targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")

//@JndiName("java:app/XDWSimulator-ejb/ModelBasedValidationWS")
public class XDWValidatorWS  extends AbstractModelBasedValidation implements Serializable{


	/** Logger */
	@Logger
	private static Log log;


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	@WebMethod
	@WebResult(name = "about")
	public String about(){
		String res = "This webservice is developped by IHE-europe / gazelle team. The aim of this validator is to validate XDW documents using model based validation.\n";
		res = res + "For more information please contact the manager of gazelle project eric.poiseau@inria.fr";
		return res;
	}
	

	@WebMethod
	@WebResult(name = "DetailedResult")
	public String validateDocument(
			@WebParam(name = "document") String document,
			@WebParam(name = "validator") String validator)
					throws SOAPException {
		Validators val = Validators.getValidatorFromValue(validator);
		String res = "No validator was found with this name : " + validator + ". The only validators accepted are " + Validators.values();
		String status = "FAILED";
		if (val != null){
			switch (val) {
			case XDW_BASIC:
				res = MicroValidation.validateXDW_BASIC(document);
				break;
			case XBER_WD:
				res = MicroValidation.validateXBER_WD(document);
				break;
			case CMPD1_WD:
				res = MicroValidation.validateCMPD1_WD(document);
				break;
			case CMPD2_WD:
				res = MicroValidation.validateCMPD2_WD(document);
				break;
			case XTHM_WD:
				res = MicroValidation.validateXTHM_WD(document);
				break;
			default:
				res = "No validator with sush name is available. The only validators accepted are " + Validators.values();
				break;
			}
			status = ((res != null) &&res.contains("<ValidationTestResult>FAILED</ValidationTestResult>"))?"FAILED":"PASSED";
		}
		this.addValidatorUsage(validator, status);
		return res;
	}

	@Override
	protected String buildReportOnParsingFailure(GazelleValidationException e, ValidatorDescription validatorDescription) {
		return null;
	}

	@Override
	protected String executeValidation(String s, ValidatorDescription validatorDescription, boolean b) throws GazelleValidationException {
		return null;
	}


	@WebResult(name = "DetailedResult")
	@WebMethod
	public String validateXDWB64(@WebParam(name="base64ObjectToValidate") String xdwDocumentB64, String validator) throws SOAPException{
		byte[] bb = Base64.decode(xdwDocumentB64);
		if (bb != null){
			String xdwDocument = new String(bb);
			return this.validateDocument(xdwDocument, validator);
		}
		else{
			return this.validateDocument(null, null);
		}
	}
	
	

	@WebMethod
	@WebResult(name = "Validators")
	public List<String> getListOfValidators(@WebParam(name="descriminator") String descriminator)
			throws SOAPException {
		List<String> res = new ArrayList<String>();
		for (Validators val : Validators.values()) {
			res.add(val.getValue());
		}
		return res;
	}

	@Override
	protected ValidatorDescription getValidatorByOidOrName(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	protected List<ValidatorDescription> getValidatorsForDescriminator(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}



	
}


