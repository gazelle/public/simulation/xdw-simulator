package net.ihe.gazelle.xdw.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xdw.TXDWWorkflowDocument;
import net.ihe.gazelle.xdw.validator.ValidatorException;

public class XDWTransformer {
	
	public static TXDWWorkflowDocument load(InputStream is) throws ValidatorException{
		final ValidatorException vexp = new ValidatorException("Errors when trying to parse the document", new ArrayList<Notification>());
		try{
			JAXBContext jc = JAXBContext.newInstance(TXDWWorkflowDocument.class);
			Unmarshaller u = jc.createUnmarshaller();
			u.setEventHandler(new ValidationEventHandler() {
				
				@Override
				public boolean handleEvent(ValidationEvent event) {
					 return XDWTransformer.handleEvent(event, vexp);
				}
			});
			TXDWWorkflowDocument res = (TXDWWorkflowDocument) u.unmarshal(is);
			return res;
		}
		catch(JAXBException e){
			throw vexp;
		}
	}
	
	protected static boolean handleEvent(ValidationEvent event, ValidatorException vexp) {
		 if (event.getSeverity() != ValidationEvent.WARNING) {  
            ValidationEventLocator vel = event.getLocator();
            Notification not = new Error();
            not.setDescription("Line:Col[" + vel.getLineNumber() +  
                    ":" + vel.getColumnNumber() +  
                    "]:" +event.getMessage());
            not.setTest("message_parsing");
            not.setLocation("All the document");
            vexp.getDiagnostic().add(not);
        }  
        return true; 
	}

	
	public static void save(OutputStream os, TXDWWorkflowDocument txdw) throws JAXBException{
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.xdw");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF8");
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.marshal(txdw, os);
	}

}
