package net.ihe.gazelle.xdw.generator.action;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.xdw.TXDWWorkflowDocument;
import net.ihe.gazelle.xdw.editor.action.xdw.XDWInitiator;
import net.ihe.gazelle.xdw.editor.action.xdw.XDWTXDWWorkflowDocumentEditorLocal;
//import net.ihe.gazelle.xdw.editor.action.xdw.XDWInitiator;
//import net.ihe.gazelle.xdw.editor.action.xdw.XDWTXDWWorkflowDocumentEditorLocal;
import net.ihe.gazelle.xdw.model.XDWDocument;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.JndiName;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Identity;

@Stateful
@Name("generatorManagerBean")
@Scope(ScopeType.SESSION)
public class GeneratorManager implements GeneratorManagerLocal {
	
	@Logger
	private static Log log;
	
	private String fileToSaveName;
	
	public String getFileToSaveName() {
		return fileToSaveName;
	}

	public void setFileToSaveName(String fileToSaveName) {
		this.fileToSaveName = fileToSaveName;
	}
	
	public void initSaveAction(){
		this.fileToSaveName = null;
	}
	
	public void saveXDWDoc(String doc){
		if (doc != null){
			String name = this.fileToSaveName;
			if (!(
					(this.fileToSaveName.split("\\.").length >=2) &&
					(this.fileToSaveName.split("\\.")[this.fileToSaveName.split("\\.").length-1].equals("xml"))
				)){
				name = name + ".xml";
			}
			this.saveDocument(doc, name);
			FacesMessages.instance().add("XDW Document was saved.");
		} else {
			FacesMessages.instance().add("The doc is empty.");
		}
	}

	public String generateXDWWorkflowDocument(){
		XDWTXDWWorkflowDocumentEditorLocal xx = (XDWTXDWWorkflowDocumentEditorLocal)(Component.getInstance("xDWTXDWWorkflowDocumentEditorManager"));
		xx.setSelectedXDWTXDWWorkflowDocument(XDWInitiator.initTXDWWorkflowDocument());
		return "/editor/xdw/TXDWWorkflowDocument.xhtml";
	}
	
	public String generateXDWWorkflowDocument(String xdwDoc){
		try{
			TXDWWorkflowDocument xdw = net.ihe.gazelle.edit.Transform.load(new ByteArrayInputStream(xdwDoc.getBytes()));
			XDWTXDWWorkflowDocumentEditorLocal xx = (XDWTXDWWorkflowDocumentEditorLocal)(Component.getInstance("xDWTXDWWorkflowDocumentEditorManager"));
			xx.setSelectedXDWTXDWWorkflowDocument(xdw);
		}
		catch(Exception e){
			XDWTXDWWorkflowDocumentEditorLocal xx = (XDWTXDWWorkflowDocumentEditorLocal)(Component.getInstance("xDWTXDWWorkflowDocumentEditorManager"));
			xx.setSelectedXDWTXDWWorkflowDocument(XDWInitiator.initTXDWWorkflowDocument());
		}
		return "/editor/xdw/TXDWWorkflowDocument.xhtml";
	}
	
	public void downloadFile(String string){
		if (string == null){
			FacesMessages.instance().add("You want to download an empty file ! Thank you to inform the administrator about this problem.");
			return;
		}
		this.showFile(string.getBytes(), "xml", "generated.xml", true);
	}
	
	public void validate(String comment) {
		// First send the file to EVSClient
		HttpClient client = new HttpClient();
		String linkEVS = ApplicationConfiguration.getValueOfVariable("evs_url");
		PostMethod filePost = new PostMethod(linkEVS + "/upload");

		// use the type as file name
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		String messageType = "comment" + sdf.format(new Date());

		PartSource partSource = new ByteArrayPartSource(messageType, comment.getBytes());
		Part[] parts = { new FilePart("message", partSource) };
		filePost.setRequestEntity(new MultipartRequestEntity(parts, filePost
				.getParams()));
		int status = -1;
		try {
			status = client.executeMethod(filePost);
		} catch (Exception e) {
			status = -1;
			e.printStackTrace();
		}
		// redirect the user to the good page
		if (status == HttpStatus.SC_OK) {
			try {
				String key = filePost.getResponseBodyAsString();
				String encodedKey = URLEncoder.encode(key, "UTF-8");
				ExternalContext extCtx = FacesContext.getCurrentInstance().getExternalContext();
				HttpServletResponse response = (HttpServletResponse) extCtx.getResponse();
				
				String url = linkEVS+ "/validate.seam?file=" + encodedKey;
				response.sendRedirect(url);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void saveDocument(String ss, String path){
		if ((ss != null) && (!ss.equals(""))){
			
			XDWDocument xx = new XDWDocument();
			xx.setAuthor(Identity.instance().getCredentials().getUsername());
			xx.setCreationDate(new Date());
			EntityManager em = (EntityManager)Component.getInstance("entityManager");
			xx = em.merge(xx);
			xx.setPath(path);
			em.flush();
			if (path == null || path.equals("")){
				xx.setPath(xx.getId() + ".xml");
				xx = em.merge(xx);
				em.flush();
			}
			saveFileOnTheDisc(xx, ss);
		}
	}
	
	private void saveFileOnTheDisc(XDWDocument xx, String content){
		if ((xx != null) && (xx.getId() != null)){
			String repo = ApplicationConfiguration.getValueOfVariable("doc_path");
			String fileName = xx.getId() + "_" + xx.getPath();
			File ff = new File(repo + "/" + fileName);
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(ff));
				bw.write(content);
				
			} catch (IOException e) {
				e.printStackTrace();
			} 
			finally{
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	private void showFile(byte[] data, String extension, String fileName, boolean download) {
	    try {
            InputStream inputStream = null;
            inputStream = new ByteArrayInputStream(data);

            if (inputStream != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                HttpServletResponse response = (HttpServletResponse) context
                        .getExternalContext().getResponse();

                int length = inputStream.available();
                byte[] bytes = new byte[length];

                if (extension.equals("jpeg") || extension.equals("jpg")) {
                    response.setContentType("image/jpeg");
                } else if (extension.equals("gif")) {
                    response.setContentType("image/gif");
                } else if (extension.equals("png")) {
                    response.setContentType("image/png");
                } else if (extension.equals("tiff")) {
                    response.setContentType("image/tiff");
                } else if (extension.equals("txt") || extension.equals("text")
                        || extension.equals("log")) {
                    response.setContentType("text/plain");
                } else if (extension.equals("tar")) {
                    response.setContentType("application/x-tar");
                } else if (extension.equals("zip")) {
                    response.setContentType("application/zip");
                } else if (extension.equals("tar")) {
                    response.setContentType("application/x-tar");
                }else if (extension.equals("pdf")) {
                    response.setContentType("application/pdf");
                } else if (extension.equals("xml") || extension.equals("xsd")
                        || extension.equals("dtd")) {
                    response.setContentType("text/xml");
                }
                else
                 {
                    response.setContentType("application/octet-stream");
                 }
                 if (fileName != null){
                     response.setHeader("Content-Disposition", "inline; filename=\"" + fileName + "\"");
                 }
                 if (download){
                     response.setHeader("Content-Disposition", "attachment;filename=\""+  fileName + "\"");
                 }
                
                
                response.setContentLength(length);

                ServletOutputStream servletOutputStream;

                servletOutputStream = response.getOutputStream();

                inputStream.read(bytes);

                servletOutputStream.write(bytes);

                servletOutputStream.flush();
                servletOutputStream.close();

                context.responseComplete();
            }

        } catch (Exception e) {
            e.printStackTrace();
            FacesMessages.instance().add(
                    "Impossible to display file : " + e.getMessage());
        }
	}
	
	
	@Destroy
	@Remove
	public void destroy(){
		log.info("destroy generatorManager..");
	}

}
