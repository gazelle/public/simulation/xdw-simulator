package net.ihe.gazelle.xdw.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;

import javax.validation.constraints.NotNull;
import org.jboss.seam.annotations.Name;

@Entity
@Name("xdwDocument")
@Table(name = "xdw_doc")
@SequenceGenerator(name = "xdw_doc_sequence", sequenceName = "xdw_doc_id_seq", allocationSize = 1)
public class XDWDocument implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "xdw_doc_sequence")
	private Integer id;

	@Column(name="path")
	private String path;
	
	@Column(name="author")
	private String author;
	
	@Column(name="creationDate")
	private Date creationDate;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XDWDocument other = (XDWDocument) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		return true;
	}
	
	public String getContent(){
		String res = "";
		String repo = ApplicationConfiguration.getValueOfVariable("doc_path");
		try (BufferedReader br = new BufferedReader(new FileReader(new File(repo + "/" + this.id + "_" + this.path)))){
			String line = br.readLine();
			while (line != null){
				res = res + line + "\n";
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public String toString() {
		return id.toString();
	}
	
	
}
