package net.ihe.gazelle.xdw.filter;

import java.io.Serializable;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("XDWFilteringBean")
@Scope(ScopeType.PAGE)
public class XDWFilteringBean<T> implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -5680001353441022183L;
    private String pathFilter;
    private String authorFilter;
	public String getPathFilter() {
		return pathFilter;
	}
	public void setPathFilter(String pathFilter) {
		this.pathFilter = pathFilter;
	}
	public String getAuthorFilter() {
		return authorFilter;
	}
	public void setAuthorFilter(String authorFilter) {
		this.authorFilter = authorFilter;
	}
   
 

    }