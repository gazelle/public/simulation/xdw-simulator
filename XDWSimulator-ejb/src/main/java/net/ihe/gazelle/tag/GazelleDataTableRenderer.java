package net.ihe.gazelle.tag;

import static org.richfaces.renderkit.RenderKitUtils.addToScriptHash;
import static org.richfaces.renderkit.RenderKitUtils.renderAttribute;
import static org.richfaces.renderkit.util.AjaxRendererUtils.buildAjaxFunction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.PartialResponseWriter;
import javax.faces.context.ResponseWriter;

import org.ajax4jsf.javascript.ScriptUtils;
import org.ajax4jsf.model.SequenceRange;
import org.richfaces.component.AbstractColumn;
import org.richfaces.component.AbstractDataTable;
import org.richfaces.component.ExtendedDataTableState;
import org.richfaces.component.SortOrder;
import org.richfaces.component.UIDataTableBase;
import org.richfaces.component.util.HtmlUtil;
import org.richfaces.context.OnOffResponseWriter;
import org.richfaces.model.SelectionMode;
import org.richfaces.renderkit.AjaxFunction;
import org.richfaces.renderkit.ComponentAttribute;
import org.richfaces.renderkit.DataTableRenderer;
import org.richfaces.renderkit.HtmlConstants;
import org.richfaces.renderkit.RenderKitUtils;
import org.richfaces.renderkit.RenderKitUtils.ScriptHashVariableWrapper;
import org.richfaces.renderkit.RowHolderBase;




public class GazelleDataTableRenderer extends DataTableRenderer {
	/*private class ClientSelection {
        // TODO nick - use enum instead of constant
        public static final String FLAG_RESET = "x";
        public static final String FLAG_ALL = "a";
        public static final String FLAG_AFTER_RANGE = "d";
        public static final String FLAG_BEFORE_RANGE = "u";
        // TODO nick - add special class that will express selection range
        private int[][] ranges;
        private int activeIndex;
        private int shiftIndex;
        private String selectionFlag;
        private int index;

        public ClientSelection(String selectionString) {
            // TODO nick - this code is not readable at all - lacks comments, has lot of arrays operation
            String[] strings = selectionString.split("\\|", -1);
            String[] rangeStrings = strings[0].split(";");
            if (strings[0].length() > 0) {
                ranges = new int[rangeStrings.length][2];
                for (int i = 0; i < rangeStrings.length; i++) {
                    String[] rangeString = rangeStrings[i].split(",");
                    ranges[i][0] = Integer.parseInt(rangeString[0]);
                    ranges[i][1] = Integer.parseInt(rangeString[1]);
                }
            } else {
                ranges = new int[0][0];
            }
            if (strings[1].matches("\\d+")) {
                activeIndex = Integer.parseInt(strings[1]);
            } else {
                activeIndex = -1;
            }
            if (strings[2].matches("\\d+")) {
                shiftIndex = Integer.parseInt(strings[2]);
            } else if (strings[2].length() > 0) {
                shiftIndex = -1;
            } else {
                shiftIndex = -2;
            }
            if (strings[3].length() > 0) {
                selectionFlag = strings[3];
            }
            index = 0;
        }

        public boolean isSelected(int index) {
            int i = 0;
            while (i < ranges.length && index >= ranges[i][0]) {
                if (index >= ranges[i][0] && index <= ranges[i][1]) {
                    return true;
                } else {
                    i++;
                }
            }
            return false;
        }

        public boolean isActiveIndex(int index) {
            return activeIndex == index;
        }

        public boolean isShiftIndex(int index) {
            return shiftIndex == index;
        }

        public boolean isCleanShiftIndex() {
            return shiftIndex == -2;
        }

        public String getSelectionFlag() {
            return selectionFlag;
        }

        public int nextIndex() {
            return index++;
        }
    }

    public GazelleDataTableRenderer() {
        System.out.println("CustomCheckboxRenderer <init>");
    }
    private void encodeEmptyFooterCell(FacesContext context, ResponseWriter writer, UIComponent column, boolean isLastColumn) throws IOException {
        if (column.isRendered()) {
            writer.startElement(HtmlConstants.TD_ELEM, column);
            if (!isLastColumn) {
                writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-td-" + column.getId(), null);
            }
            writer.startElement(HtmlConstants.DIV_ELEM, column);
            writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-ftr-c-emp rf-edt-c-" + column.getId(), null);
            writer.endElement(HtmlConstants.DIV_ELEM);
            writer.endElement(HtmlConstants.TD_ELEM);
        }
    }

    private void encodeHeaderOrFooterCell(FacesContext context, ResponseWriter writer, UIComponent column, String facetName, boolean isLastColumn)
        throws IOException {
        if (column.isRendered()) {

            String classAttribute = facetName + "Class";
            boolean useBuiltInSort = "header".equals(facetName) && column instanceof AbstractColumn && ((AbstractColumn) column).useBuiltInSort();
            writer.startElement(HtmlConstants.TD_ELEM, column);
            if (!isLastColumn) {
                writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-td-" + column.getId(), null);
            }
            if ("header".equals(facetName)) {
                writer.startElement(HtmlConstants.DIV_ELEM, column);
                writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-rsz-cntr rf-edt-c-" + column.getId(), null);
                writer.startElement(HtmlConstants.DIV_ELEM, column);
                writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-rsz", null);
                writer.endElement(HtmlConstants.DIV_ELEM);
                writer.endElement(HtmlConstants.DIV_ELEM);
            }

            writer.startElement(HtmlConstants.DIV_ELEM, column);
            writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, HtmlUtil.concatClasses("rf-edt-"
                + getFacetClassName(facetName) + "-c", "rf-edt-c-" + column.getId()), null);
            writer.startElement(HtmlConstants.DIV_ELEM, column);
            String columnHeaderClass = "rf-edt-" + getFacetClassName(facetName) + "-c-cnt";
            if (useBuiltInSort) {
                columnHeaderClass = HtmlUtil.concatClasses( columnHeaderClass, "rf-edt-c-srt");
            }
            columnHeaderClass = HtmlUtil.concatClasses( columnHeaderClass, column.getAttributes().get(classAttribute));
            writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, columnHeaderClass, null);
            UIComponent facet = column.getFacet(facetName);
            if (facet != null && facet.isRendered()) {
                facet.encodeAll(context);
            }

            if ("header".equals(facetName) && column instanceof AbstractColumn && ((AbstractColumn) column).useBuiltInSort()) {
                writer.startElement(HtmlConstants.SPAN_ELEM, column);
                String classAttr = "rf-edt-srt rf-edt-srt-btn ";
                SortOrder sortOrder = (SortOrder) column.getAttributes().get("sortOrder");
                if (sortOrder == null || sortOrder == SortOrder.unsorted) {
                    classAttr = classAttr + "rf-edt-srt-uns";
                } else if (sortOrder == SortOrder.ascending) {
                    classAttr = classAttr + "rf-edt-srt-asc";
                } else if (sortOrder == SortOrder.descending) {
                    classAttr = classAttr + "rf-edt-srt-des";
                }
                writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, classAttr, null);
                writer.writeAttribute("data-columnid", column.getId(), null);
                writer.endElement(HtmlConstants.SPAN_ELEM);
            }

            writer.endElement(HtmlConstants.DIV_ELEM);
            writer.endElement(HtmlConstants.DIV_ELEM);
            writer.endElement(HtmlConstants.TD_ELEM);
        }
    }
    private static final Map<java.lang.String, org.richfaces.renderkit.ComponentAttribute> EVENT_ATTRIBUTES = Collections
        .unmodifiableMap(ComponentAttribute.createMap(
            new ComponentAttribute("onselectionchange").setEventNames(new String[] { "selectionchange" }),
            new ComponentAttribute("onbeforeselectionchange").setEventNames(new String[] { "beforeselectionchange" }),
            new ComponentAttribute("onready").setEventNames(new String[] { "ready" })));
    private void updateWidthOfColumns(FacesContext context, UIComponent component, String widthString) {
        if (widthString != null && widthString.length() > 0) {
            String[] widthArray = widthString.split(",");
            for (int i = 0; i < widthArray.length; i++) {
                String[] widthEntry = widthArray[i].split(":");
                UIComponent column = component.findComponent(widthEntry[0]);
                updateAttribute(context, column, "width", widthEntry[1]);
            }
        }
    }

    private void updateColumnsOrder(FacesContext context, UIComponent component, String columnsOrderString) {
        if (columnsOrderString != null && columnsOrderString.length() > 0) {
            String[] columnsOrder = columnsOrderString.split(",");
            updateAttribute(context, component, "columnsOrder", columnsOrder);
            context.getPartialViewContext().getRenderIds().add(component.getClientId(context)); // TODO Use partial re-rendering here.
        }
    }
    protected static enum PartName {

        frozen,
        normal;
        private String id;

        private PartName() {
            id = String.valueOf(this.toString().charAt(0));
        }

        public String getId() {
            return id;
        }
    }
    protected final class Part {
        private PartName name;
        private List<UIComponent> columns;

        public Part(PartName name, List<UIComponent> columns) {
            this.name = name;
            this.columns = columns;
        }

        public PartName getName() {
            return name;
        }

        public List<UIComponent> getColumns() {
            return columns;
        }
    }
    protected enum EncoderVariance {
        full {
            public void encodeStartUpdate(FacesContext context, String targetId) throws IOException {
                // do nothing
            }

            public void encodeEndUpdate(FacesContext context) throws IOException {
                // do nothing
            }
        },
        partial {
            private void switchResponseWriter(FacesContext context, boolean writerState) {
                ResponseWriter writer = context.getResponseWriter();
                ((OnOffResponseWriter) writer).setSwitchedOn(writerState);
            }

            public void encodeStartUpdate(FacesContext context, String targetId) throws IOException {
                switchResponseWriter(context, true);

                context.getPartialViewContext().getPartialResponseWriter().startUpdate(targetId);
            }

            public void encodeEndUpdate(FacesContext context) throws IOException {
                context.getPartialViewContext().getPartialResponseWriter().endUpdate();

                switchResponseWriter(context, false);
            }
        };

        public abstract void encodeStartUpdate(FacesContext context, String targetId) throws IOException;

        public abstract void encodeEndUpdate(FacesContext context) throws IOException;

    }
    protected class RendererState extends RowHolderBase {
        private UIDataTableBase table;
        private List<Part> parts;
        private Part current;
        private Iterator<Part> partIterator;
        private EncoderVariance encoderVariance = EncoderVariance.full;

        public RendererState(FacesContext context, UIDataTableBase table) {
            super(context);
            this.table = table;

            List<UIComponent> columns = getOrderedColumns(context);

          

         
    
            parts = new ArrayList<Part>(PartName.values().length);
   
            if (columns.size() > 0) {
                parts.add(new Part(PartName.normal, columns));
            }
        }

        protected List<UIComponent> getOrderedColumns(FacesContext context) {
            Map<String, UIComponent> columnsMap = new LinkedHashMap<String, UIComponent>();
            Iterator<UIComponent> iterator = table.columns();
            while (iterator.hasNext()) { // initialize a map of all the columns
                UIComponent component = iterator.next();
                if (component.isRendered()) {
                    columnsMap.put(component.getId(), component);
                }
            }

            List<UIComponent> columns = new ArrayList<UIComponent>();

            String[] columnsOrder = RenderKitUtils.evaluateAttribute("columnsOrder", table, context);
            if (columnsOrder != null && columnsOrder.length > 0) { // add columns in the order specified by columnsOrder
                for (int i = 0; i < columnsOrder.length && !columnsMap.isEmpty(); i++) {
                    columns.add(columnsMap.remove(columnsOrder[i]));
                }
            }
            for (UIComponent column : columnsMap.values()) { // add the remaining columns
                columns.add(column);
            }

            return columns;
        }

        public UIDataTableBase getRow() {
            return table;
        }

        public void startIterate() {
            partIterator = parts.iterator();
        }

        public Part nextPart() {
            current = partIterator.next();
            return current;
        }

        public Part getPart() {
            return current;
        }

        public boolean hasNextPart() {
            return partIterator.hasNext();
        }

        public EncoderVariance getEncoderVariance() {
            return encoderVariance;
        }

        public void setEncoderVariance(EncoderVariance encoderVariance) {
            this.encoderVariance = encoderVariance;
        }
    }
    private void encodeHeaderOrFooter(RendererState state, String facetName) throws IOException {
        FacesContext context = state.getContext();
        ResponseWriter writer = context.getResponseWriter();
        UIDataTableBase table = state.getRow();
        boolean columnFacetPresent = table.isColumnFacetPresent(facetName);
        if (columnFacetPresent || "footer".equals(facetName)) {
            writer.startElement(HtmlConstants.DIV_ELEM, table);
            writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-" + getFacetClassName(facetName), null);
            writer.startElement(HtmlConstants.TABLE_ELEMENT, table);
            writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-tbl", null);
            writer.startElement(HtmlConstants.TBODY_ELEMENT, table);
            writer.startElement(HtmlConstants.TR_ELEMENT, table);
            String clientId = table.getClientId(context);
            for (state.startIterate(); state.hasNextPart();) {
                Part part = state.nextPart();
                PartName partName = part.getName();
                Iterator<UIComponent> columns = part.getColumns().iterator();
                if (columns.hasNext()) {
                    writer.startElement(HtmlConstants.TD_ELEM, table);
                    if (PartName.frozen.equals(partName) && "footer".equals(facetName)) {
                        writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-ftr-fzn", null);
                    }
                    writer.startElement(HtmlConstants.DIV_ELEM, table);
                    if (PartName.frozen.equals(partName)) {
                        if ("header".equals(facetName)) {
                            writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, clientId + ":frozenHeader", null);
                        }
                    } else {
                        writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, clientId + ":" + facetName, null);
                        writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-cnt"
                            + ("footer".equals(facetName) ? " rf-edt-ftr-cnt" : ""), null);
                    }

                    String tableId = clientId + ":cf" + facetName.charAt(0) + partName.getId();
                    EncoderVariance encoderVariance = state.getEncoderVariance();
                    encoderVariance.encodeStartUpdate(context, tableId);

                    writer.startElement(HtmlConstants.TABLE_ELEMENT, table);
                    writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, tableId, null);
                    writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-tbl", null);
                    writer.startElement(HtmlConstants.TBODY_ELEMENT, table);
                    writer.startElement(HtmlConstants.TR_ELEMENT, table);
                    int columnNumber = 0;
                    boolean filterRowRequired = false;
                    int lastColumnNumber = part.getColumns().size() - 1;
                    while (columns.hasNext()) {
                        UIComponent column = columns.next();
                        if (!filterRowRequired && "header".equals(facetName) && column instanceof AbstractColumn && ((AbstractColumn) column).useBuiltInFilter()) {
                            filterRowRequired = true;
                        }
                        if (columnFacetPresent) {
                            encodeHeaderOrFooterCell(context, writer, column, facetName, columnNumber == lastColumnNumber);
                        } else {
                            encodeEmptyFooterCell(context, writer, column, columnNumber == lastColumnNumber);
                        }
                        columnNumber++;
                    }
                    writer.endElement(HtmlConstants.TR_ELEMENT);
                    if (filterRowRequired) {  // filter row
                        writer.startElement(HtmlConstants.TR_ELEMENT, table);
                        columns = part.getColumns().iterator();
                        while (columns.hasNext()) {
                            UIComponent column = columns.next();
                            if (column.isRendered()) {
                                writer.startElement(HtmlConstants.TD_ELEM, column);
                                writer.startElement(HtmlConstants.DIV_ELEM, column);
                                writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-flt-c rf-edt-c-" + column.getId(), null);
                                writer.startElement(HtmlConstants.DIV_ELEM, column);
                                writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-flt-cnt", null);
                                if (column.getAttributes().get("filterField") != null &&  ! "custom".equals(column.getAttributes().get("filterType"))) {
                                    writer.startElement(HtmlConstants.INPUT_ELEM, column);
                                    writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, clientId + ":" + column.getId() + ":flt", null);
                                    writer.writeAttribute(HtmlConstants.NAME_ATTRIBUTE, clientId + ":" + column.getId() + ":flt", null);
                                    String inputClass = "rf-edt-flt-i";
                                    List<FacesMessage> messages = context.getMessageList(column.getClientId());
                                    if (! messages.isEmpty()) {
                                        inputClass += " rf-edt-flt-i-err";
                                        writer.writeAttribute("value", column.getAttributes().get("submittedFilterValue"), null);
                                    } else {
                                        writer.writeAttribute("value", column.getAttributes().get("filterValue"), null);
                                    }
                                    writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, inputClass, null);
                                    writer.writeAttribute("data-columnid", column.getId(), null);
                                    writer.endElement(HtmlConstants.INPUT_ELEM);
                                }
                                writer.endElement(HtmlConstants.DIV_ELEM);
                                writer.endElement(HtmlConstants.DIV_ELEM);
                                writer.endElement(HtmlConstants.TD_ELEM);
                            }
                        }
                        writer.endElement(HtmlConstants.TR_ELEMENT);
                    }
                    writer.endElement(HtmlConstants.TBODY_ELEMENT);
                    writer.endElement(HtmlConstants.TABLE_ELEMENT);

                    encoderVariance.encodeEndUpdate(context);

                    writer.endElement(HtmlConstants.DIV_ELEM);

                    writer.endElement(HtmlConstants.TD_ELEM);
                }
            }
            writer.endElement(HtmlConstants.TR_ELEMENT);
            // the start of the scroller
            if ("footer".equals(facetName)) {
                int frozenColumns = 0;
                int scrollingColumns = 0;
                for (state.startIterate(); state.hasNextPart();) {
                    Part part = state.nextPart();
                    PartName partName = part.getName();
                    Iterator<UIComponent> columns = part.getColumns().iterator();
                    if (columns.hasNext()) {
                        if (PartName.frozen.equals(partName)) {
                            frozenColumns += 1;
                        } else {
                            scrollingColumns += 1;
                        }
                    }
                }
                writer.startElement(HtmlConstants.TR_ELEMENT, table);
                if (frozenColumns > 0) {
                    writer.startElement(HtmlConstants.TD_ELEM, table);
                    writer.writeAttribute(HtmlConstants.COLSPAN_ATTRIBUTE, frozenColumns, null);
                    writer.endElement(HtmlConstants.TD_ELEM);
                }
                if (scrollingColumns > 0) {
                    writer.startElement(HtmlConstants.TD_ELEM, table);
                    writer.writeAttribute(HtmlConstants.COLSPAN_ATTRIBUTE, scrollingColumns, null);
                    writer.startElement(HtmlConstants.DIV_ELEM, table);
                    writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, clientId + ":scrl", null);
                    writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-scrl", null);
                    writer.startElement(HtmlConstants.DIV_ELEM, table);
                    writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, clientId + ":scrl-cnt", null);
                    writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-scrl-cnt", null);
                    writer.endElement(HtmlConstants.DIV_ELEM);
                    writer.endElement(HtmlConstants.DIV_ELEM);
                    writer.endElement(HtmlConstants.TD_ELEM);
                }
                writer.endElement(HtmlConstants.TR_ELEMENT);
            }
            // the end of the scroller
            writer.endElement(HtmlConstants.TBODY_ELEMENT);
            writer.endElement(HtmlConstants.TABLE_ELEMENT);
            writer.endElement(HtmlConstants.DIV_ELEM);
        }
    }

    public void encodeHeader(RendererState state) throws IOException {
        FacesContext context = state.getContext();
        ResponseWriter writer = context.getResponseWriter();
        UIDataTableBase table = state.getRow();

        UIComponent header = table.getFacet("header");
        if (header != null && header.isRendered()) {
            String elementId = table.getClientId(context) + ":tfh";

            EncoderVariance encoderVariance = state.getEncoderVariance();
            encoderVariance.encodeStartUpdate(context, elementId);

            writer.startElement(HtmlConstants.DIV_ELEM, table);
            writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, elementId, null);
            writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE,
                HtmlUtil.concatClasses("rf-edt-tbl-hdr", table.getHeaderClass()), null);
            header.encodeAll(context);
            writer.endElement(HtmlConstants.DIV_ELEM);

            encoderVariance.encodeEndUpdate(context);
        }

        encodeHeaderOrFooter(state, "header");
    }

    public void encodeBody(RendererState state) throws IOException {
        FacesContext context = state.getContext();
        ResponseWriter writer = context.getResponseWriter();
        UIDataTableBase table = state.getRow();
        String clientId = table.getClientId(context);
        String tableBodyId = clientId + ":b";
        EncoderVariance encoderVariance = state.getEncoderVariance();
        encoderVariance.encodeStartUpdate(context, tableBodyId);
        writer.startElement(HtmlConstants.DIV_ELEM, table);
        writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, tableBodyId, null);
        writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-b", null);
        if (table.getRowCount() == 0) {
            UIComponent facet = table.getFacet("noData");
            writer.startElement(HtmlConstants.DIV_ELEM, table);
            writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-ndt", null);
            if (facet != null && facet.isRendered()) {
                facet.encodeAll(context);
            } else {
                Object noDataLabel = table.getAttributes().get("noDataLabel");
                if (noDataLabel != null) {
                    writer.writeText(noDataLabel, "noDataLabel");
                }
            }
            writer.endElement(HtmlConstants.DIV_ELEM);
        } else {
            table.getAttributes().put("clientFirst", 0);
            writer.startElement(HtmlConstants.DIV_ELEM, table);
            writer.startElement(HtmlConstants.DIV_ELEM, table);
            writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-spcr", null);
            writer.endElement(HtmlConstants.DIV_ELEM);
            writer.startElement(HtmlConstants.TABLE_ELEMENT, table);
            writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-tbl", null);
            writer.startElement(HtmlConstants.TBODY_ELEMENT, table);
            writer.startElement(HtmlConstants.TR_ELEMENT, table);
            for (state.startIterate(); state.hasNextPart();) {
                writer.startElement(HtmlConstants.TD_ELEM, table);
                writer.startElement(HtmlConstants.DIV_ELEM, table);
                PartName partName = state.nextPart().getName();
                if (PartName.normal.equals(partName)) {
                    writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, clientId + ":body", null);
                    writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-cnt", null);
                }
                String targetId = clientId + ":tbt" + partName.getId();
                writer.startElement(HtmlConstants.TABLE_ELEMENT, table);
                writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, targetId, null);
                writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-tbl", null);
                writer.startElement(HtmlConstants.TBODY_ELEMENT, table);
                writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, clientId + ":tb" + partName.getId(), null);
                encodeRows(context, state);
                writer.endElement(HtmlConstants.TBODY_ELEMENT);
                writer.endElement(HtmlConstants.TABLE_ELEMENT);

                writer.endElement(HtmlConstants.DIV_ELEM);
                writer.endElement(HtmlConstants.TD_ELEM);
            }
            writer.endElement(HtmlConstants.TR_ELEMENT);
            writer.endElement(HtmlConstants.TBODY_ELEMENT);
            writer.endElement(HtmlConstants.TABLE_ELEMENT);
            writer.endElement(HtmlConstants.DIV_ELEM);
        }
        writer.endElement(HtmlConstants.DIV_ELEM);
        encoderVariance.encodeEndUpdate(context);
    }

    public void encodeFooter(RendererState state) throws IOException {
        FacesContext context = state.getContext();
        ResponseWriter writer = context.getResponseWriter();
        UIDataTableBase table = state.getRow();

        encodeHeaderOrFooter(state, "footer");

        UIComponent footer = table.getFacet("footer");
        if (footer != null && footer.isRendered()) {
            String elementId = table.getClientId(context) + ":tff";

            EncoderVariance encoderVariance = state.getEncoderVariance();
            encoderVariance.encodeStartUpdate(context, elementId);

            writer.startElement(HtmlConstants.DIV_ELEM, table);
            writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, elementId, null);
            writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE,
                HtmlUtil.concatClasses("rf-edt-tbl-ftr", table.getFooterClass()), null);
            footer.encodeAll(context);
            writer.endElement(HtmlConstants.DIV_ELEM);

            encoderVariance.encodeEndUpdate(context);
        }
    }
    private String getFacetClassName(String name) {
        if ("header".equals(name)) {
            return "hdr";
        } else if ("footer".equals(name)) {
            return "ftr";
        }

        throw new IllegalArgumentException(name);
    }
    public RendererState createRowHolder(FacesContext context, UIComponent component, Object[] options) {
        return new RendererState(context, (UIDataTableBase) component);
    }
    public void encodeMetaComponent(FacesContext context, UIComponent component, String metaComponentId) throws IOException {
        AbstractDataTable table = (AbstractDataTable) component;
            ResponseWriter initialWriter = context.getResponseWriter();
            assert !(initialWriter instanceof OnOffResponseWriter);

            try {
                context.setResponseWriter(new OnOffResponseWriter(initialWriter));

                RendererState state = createRowHolder(context, component, null);
                state.setEncoderVariance(EncoderVariance.partial);

                PartialResponseWriter writer = context.getPartialViewContext().getPartialResponseWriter();

                if (UIDataTableBase.HEADER.equals(metaComponentId)) {
                    encodeHeader(state);
                    writer.startEval();
                    writer.write("jQuery("
                        + ScriptUtils.toScript('#' + ScriptUtils.escapeCSSMetachars(table.getClientId(context)))
                        + ").triggerHandler('rich:onajaxcomplete', {reinitializeHeader: true});");
                    writer.endEval();
                } else if (UIDataTableBase.FOOTER.equals(metaComponentId)) {
                    encodeFooter(state);
                } else if (UIDataTableBase.BODY.equals(metaComponentId)) {
                    encodeBody(state);
                    String clientId = table.getClientId(context);
                    writer.startUpdate(clientId + ":si");
                    encodeSelectionInput(writer, context, component);
                    writer.endUpdate();
                    writer.startEval();
                    writer.write("jQuery(" + ScriptUtils.toScript('#' + ScriptUtils.escapeCSSMetachars(clientId))
                        + ").triggerHandler('rich:onajaxcomplete', { rowCount: "
                        + getRowCount(component) + ", reinitializeBody: true});");
                    writer.endEval();
                } else {
                    throw new IllegalArgumentException("Unsupported metaComponentIdentifier: " + metaComponentId);
                }
            } finally {
                context.setResponseWriter(initialWriter);
            }
    }
    

    public void decodeMetaComponent(FacesContext context, UIComponent component, String metaComponentId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void doDecode(FacesContext context, UIComponent component) {
        super.decode(context, component);
        Map<String, String> map = context.getExternalContext().getRequestParameterMap();
        String clientId = component.getClientId(context);
        updateWidthOfColumns(context, component, map.get(clientId + ":wi"));
        if (map.get(clientId) != null) {
            updateColumnsOrder(context, component, map.get("rich:columnsOrder"));
        }

        decodeSortingFiltering(context, component);

        
    }
    public void consumeTableState(FacesContext facesContext, UIDataTableBase table, ExtendedDataTableState tableState) {
        Iterator<UIComponent> columns = table.columns();

        // width, filter, sort
        while (columns.hasNext()) {
            UIComponent component = columns.next();
            if (component instanceof AbstractColumn) {
                AbstractColumn column = (AbstractColumn) component;

                String width = tableState.getColumnWidth(column);
                if (width != null && ! width.equals(column.getWidth())) {
                    updateAttribute(facesContext, column, "width", width);
                }

                String stateFilterValue = tableState.getColumnFilter(column);
                if ( stateFilterValue != null &&  (column.getFilterValue() == null || ! column.getFilterValue().toString().equals(stateFilterValue))) {
                        updateAttribute(facesContext, column, "filterValue", stateFilterValue);
                }

                String sort = tableState.getColumnSort(column);
                if (sort != null) {
                    SortOrder sortOrder = SortOrder.valueOf(sort);
                    if (! sortOrder.equals(column.getSortOrder())) {
                        updateAttribute(facesContext, column, "sortOrder", sortOrder);
                    }
                }
            }
        }

        //order
        String[] columnsOrder = tableState.getColumnsOrder();
        if (columnsOrder != null) {
            updateAttribute(facesContext, table, "columnsOrder", columnsOrder);
        }

    }
@Override
protected void doEncodeBegin(ResponseWriter writer, FacesContext context, UIComponent component) throws IOException {
    UIDataTableBase dataTable = (UIDataTableBase) component;
    encodeTableStart(writer, context, dataTable);
    encodeTableFacets(writer, context, dataTable);
    String savedTableState = (String) component.getAttributes().get("tableState");
    if (savedTableState != null && ! savedTableState.isEmpty()) { // retrieve table state
        ExtendedDataTableState tableState = new ExtendedDataTableState(savedTableState);
        consumeTableState(context, (UIDataTableBase) component, tableState);
    }

    Map<String, Object> attributes = component.getAttributes();
    writer.startElement(HtmlConstants.DIV_ELEM, component);
    writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, component.getClientId(context), null);
    writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE,
        HtmlUtil.concatClasses("rf-edt", (String) attributes.get("styleClass")), null);
    renderAttribute(context, HtmlConstants.STYLE_ATTRIBUTE, attributes.get("style"));
}
protected void encodeSelectionInput(ResponseWriter writer, FacesContext context, UIComponent component) throws IOException {
    writer.startElement(HtmlConstants.INPUT_ELEM, component);
    writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, component.getClientId(context) + ":si", null);
    writer.writeAttribute(HtmlConstants.NAME_ATTRIBUTE, component.getClientId(context) + ":si", null);
    writer.writeAttribute(HtmlConstants.TYPE_ATTR, HtmlConstants.INPUT_TYPE_HIDDEN, null);
    UIDataTableBase table = (UIDataTableBase) component;
    StringBuilder builder = new StringBuilder("|");
    Object key = table.getRowKey();
    table.captureOrigValue(context);
    SequenceRange range = (SequenceRange) table.getComponentState().getRange();
    int first = range.getFirstRow();
    int last = first + range.getRows() - 1;
    Map<String, Object> attributes = component.getAttributes();
    table.setRowKey(attributes.get("activeRowKey"));
    int activeIndex = table.getRowIndex();
    if (activeIndex > 0) {
        if (activeIndex < first) {
            builder.append(ClientSelection.FLAG_BEFORE_RANGE);
        } else if (activeIndex > last) {
            builder.append(ClientSelection.FLAG_AFTER_RANGE);
        }
    }
    builder.append("|");
    table.setRowKey(attributes.get("shiftRowKey"));
    int shiftIndex = table.getRowIndex();
    if (shiftIndex > 0) {
        if (shiftIndex < first) {
            builder.append(ClientSelection.FLAG_BEFORE_RANGE);
        } else if (shiftIndex > last) {
            builder.append(ClientSelection.FLAG_AFTER_RANGE);
        }
    }
    builder.append("|");
    table.setRowKey(context, key);
    table.restoreOrigValue(context);
    writer.writeAttribute(HtmlConstants.VALUE_ATTRIBUTE, builder.toString(), null);
    writer.endElement(HtmlConstants.INPUT_ELEM);
}
@Override
    protected void doEncodeEnd(ResponseWriter writer, FacesContext context, UIComponent component) throws IOException {
	System.out.println("CustomRenderer doEncodeEnd");
        writer.startElement(HtmlConstants.TABLE_ELEMENT, component);
        String clientId = component.getClientId(context);
        writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, clientId + ":r", null);
        writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-rord rf-edt-tbl", null);
        writer.startElement(HtmlConstants.TR_ELEMENT, component);
        writer.startElement(HtmlConstants.TH_ELEM, component);
        writer.write("&#160;");
        writer.endElement(HtmlConstants.TH_ELEM);
        writer.endElement(HtmlConstants.TR_ELEMENT);
        for (int i = 0; i < 6; i++) {
            writer.startElement(HtmlConstants.TR_ELEMENT, component);
            writer.startElement(HtmlConstants.TD_ELEM, component);
            writer.write("&#160;");
            writer.endElement(HtmlConstants.TD_ELEM);
            writer.endElement(HtmlConstants.TR_ELEMENT);
        }
        writer.endElement(HtmlConstants.TABLE_ELEMENT);
        writer.startElement(HtmlConstants.DIV_ELEM, component);
        writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, clientId + ":d", null);
        writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-rsz-mkr", null);
        writer.endElement(HtmlConstants.DIV_ELEM);
        writer.startElement(HtmlConstants.DIV_ELEM, component);
        writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, clientId + ":rm", null);
        writer.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "rf-edt-rord-mkr", null);
        writer.endElement(HtmlConstants.DIV_ELEM);
        writer.startElement(HtmlConstants.INPUT_ELEM, component);
        writer.writeAttribute(HtmlConstants.ID_ATTRIBUTE, clientId + ":wi", null);
        writer.writeAttribute(HtmlConstants.NAME_ATTRIBUTE, clientId + ":wi", null);
        writer.writeAttribute(HtmlConstants.TYPE_ATTR, HtmlConstants.INPUT_TYPE_HIDDEN, null);
        writer.endElement(HtmlConstants.INPUT_ELEM);
        encodeSelectionInput(writer, context, component);
        AjaxFunction ajaxFunction = buildAjaxFunction(context, component);

        Map<String, Object> attributes = component.getAttributes();
        Map<String, Object> options = new HashMap<String, Object>();
        addToScriptHash(options, "selectionMode", attributes.get("selectionMode"), SelectionMode.multiple);
        addToScriptHash(options, "onbeforeselectionchange",
            RenderKitUtils.getAttributeAndBehaviorsValue(context, component, EVENT_ATTRIBUTES.get("onbeforeselectionchange")),
            null, ScriptHashVariableWrapper.eventHandler);
        addToScriptHash(options, "onselectionchange",
            RenderKitUtils.getAttributeAndBehaviorsValue(context, component, EVENT_ATTRIBUTES.get("onselectionchange")),
            null, ScriptHashVariableWrapper.eventHandler);
        addToScriptHash(options, "onready",
            RenderKitUtils.getAttributeAndBehaviorsValue(context, component, EVENT_ATTRIBUTES.get("onready")),
            null, ScriptHashVariableWrapper.eventHandler);
        StringBuilder builder = new StringBuilder("new RichFaces.ui.ExtendedDataTable('");
        builder.append(clientId).append("', ").append(getRowCount(component)).append(", function(event, clientParams) {")
            .append(ajaxFunction.toScript()).append(";}");
        if (!options.isEmpty()) {
            builder.append(",").append(ScriptUtils.toScript(options));
        }
        builder.append(");");
        getUtils().writeScript(context, component, builder.toString());
        writer.endElement(HtmlConstants.DIV_ELEM);
    }
  
private int getRowCount(UIComponent component) {
    UIDataTableBase table = (UIDataTableBase) component;
    int rows = table.getRows();
    int rowCount = table.getRowCount() - table.getFirst();
    if (rows > 0) {
        rows = Math.min(rows, rowCount);
    } else {
        rows = rowCount;
    }

    return rows;
}*/
}