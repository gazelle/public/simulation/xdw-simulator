<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="html" media-type="text/html"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Feb 02, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> Abderrazek Boufahja, IHE Development, Kereval</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <html>
            <head>
                <title>External Validation Report</title>
                <link href="resultStyle.css" rel="stylesheet" type="text/css" media="screen" />                
            </head>
            <body>
                <h2>External Validation Report</h2>
                <br/>
                <div class="rich-panel">
                    <div class="rich-panel-header">General Information</div>
                    <div class="rich-panel-body">
                        <table border="0">
                            <tr>
                                <td><b>Validation Date</b></td>
                                <td><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationDate"/> - <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTime"/></td> 
                            </tr>
                            <tr>
                                <td><b>Validation Service</b></td>
                                <td><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationServiceName"/> (<xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationServiceVersion"/>)</td>         
                            </tr>
                            <tr>
                                <td><b>Validation Test Status</b></td>
                                <td>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'PASSED')">
                                        <div class="PASSED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'FAILED')">
                                        <div class="FAILED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'ABORTED')">
                                        <div class="ABORTED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br/>
                
                <xsl:if test="count(detailedResult/DocumentValidXDW) = 1">
                    <div class="rich-panel">
                        <div class="rich-panel-header">XSD Validation detailed Results</div>
                        <div class="rich-panel-body">
                            <i>The document you have validated is an XML document. The validator has checked that it is well-formed and has validated it against one ore several XSD schemas, results of those validations are gathered in this part.</i>
                            <xsl:choose>
                                <xsl:when test="detailedResult/DocumentWellFormed/Result = 'PASSED'">
                                    <p class="PASSED">The XML document is well-formed</p>                        
                                </xsl:when>
                                <xsl:otherwise>
                                    <p class="FAILED">The XML document is not well-formed</p>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:if test="count(detailedResult/DocumentValidXDW) = 1">
                                <xsl:choose>
                                    <xsl:when test="detailedResult/DocumentValidXDW/Result = 'PASSED'">
                                        <p class="PASSED">The XML document is a valid XDW regarding schema</p>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <p class="FAILED">The XML document is not a valid XDW regarding schema because of the following reasons: </p>
                                        <xsl:if test="count(detailedResult/DocumentValidXDW/*) &gt; 3">
                                            <ul>
                                                <xsl:for-each select="detailedResult/DocumentValidXDW/*">
                                                    <xsl:if test="contains(current(), 'error')">
                                                        <li><xsl:value-of select="current()"/></li>
                                                    </xsl:if>
                                                </xsl:for-each>
                                            </ul>
                                        </xsl:if>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </div>
                    </div>
                </xsl:if>
                <br/>
                <br/>
                <div class="rich-panel">
                    <div class="rich-panel-header">Validation details</div>
                    <div class="rich-panel-body">
                        <xsl:if test="count(detailedResult/MDAValidation/Error) &gt; 0">
                            <p id="errors"><b>Errors</b></p>
                            <xsl:for-each select="detailedResult/MDAValidation/Error">
                                <table class="Error" width="98%">
                                    <tr>
                                        <td valign="top" width="100"><b>Test</b></td>
                                        <td><xsl:value-of select="Test"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Location</b></td>
                                        <td><xsl:value-of select="Location"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Description</b></td>
                                        <td><xsl:value-of select="Description"/></td>
                                    </tr>
                                </table>
                                <br/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="count(detailedResult/MDAValidation/Warning) &gt; 0">
                            <p id="warnings"><b>Warnings</b></p>
                            <xsl:for-each select="detailedResult/MDAValidation/Warning">
                                <table class="Warning" width="98%">
                                    <tr>
                                        <td valign="top" width="100"><b>Test</b></td>
                                        <td><xsl:value-of select="Test"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Location</b></td>
                                        <td><xsl:value-of select="Location"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Description</b></td>
                                        <td><xsl:value-of select="Description"/></td>
                                    </tr>
                                </table>
                                <br/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="count(detailedResult/MDAValidation/Note) &gt; 0">
                            <p id="notes"><b>INFO</b></p>
                            <xsl:for-each select="detailedResult/MDAValidation/Note">
                                <table class="Report" width="98%">
                                    <tr>
                                        <td valign="top" width="100"><b>Test</b></td>
                                        <td><xsl:value-of select="Test"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Location</b></td>
                                        <td><xsl:value-of select="Location"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Description</b></td>
                                        <td><xsl:value-of select="Description"/></td>
                                    </tr>
                                </table>
                                <br/>
                            </xsl:for-each>
                        </xsl:if>
                        
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>