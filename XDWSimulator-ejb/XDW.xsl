<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ns3="urn:ihe:iti:2011:xdw"
    xmlns:ns1="urn:hl7-org:v3"
    xmlns:ns2="http://docs.oasis-open.org/ns/bpel4people/ws-humantask/types/200803">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"
        omit-xml-declaration="yes"/>
            
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Feb 10, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> Abderrazek Boufahja, IHE-Europe Development, Kereval</xd:p>
        </xd:desc>
    </xd:doc>
    <!-- CDA document -->

    <xsl:variable name="tableWidth">50%</xsl:variable>

    <xsl:variable name="title">
        <xsl:choose>
            <xsl:when test="/ns3:XDW.WorkflowDocument/ns3:title">
                <xsl:value-of select="/ns3:XDW.WorkflowDocument/ns3:title"/>
            </xsl:when>
            <xsl:otherwise>XDW Document</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:template match="/">
        <xsl:apply-templates select="ns3:XDW.WorkflowDocument"/>
    </xsl:template>

    <xsl:template match="ns3:XDW.WorkflowDocument">
        <html>
            <head>
                <!-- <meta name='Generator' content='&CDA-Stylesheet;'/> -->
                <title>
                    <xsl:value-of select="$title"/>
                </title>
            </head>
            <body>

                <h2 align="center">
                    <xsl:value-of select="$title"/>
                </h2>
                
                <xsl:call-template name="generalInformation"/>
                
                <xsl:call-template name="patient"/>
                
                <xsl:call-template name="listAuthors"/>
                
                <h2>List of Tasks</h2>
                <xsl:if test="count(/ns3:XDW.WorkflowDocument/ns3:TaskList) = 1">
                    <xsl:for-each select="/ns3:XDW.WorkflowDocument/ns3:TaskList/ns3:XDWTask">
                        <div style="margin-left:10px;margin-right:10px;margin-bottom:10px;background-color:#aaddff;width:99%">
                            <div style="margin-left:10;">
                                <xsl:if test="count(ns3:taskData) = 1">

                                    <h3>Task 
                                        <xsl:if test="count(ns3:taskData/ns3:taskDetails) = 1">
                                            <xsl:value-of select="ns3:taskData/ns3:taskDetails/ns2:id"/> :
                                            <xsl:value-of select="ns3:taskData/ns3:taskDetails/ns2:name"/>
                                        </xsl:if>
                                    </h3>
                                    <xsl:if test="count(ns3:taskData/ns3:taskDetails) = 1">
                                        <b>Task Type : </b>
                                        <xsl:value-of select="ns3:taskData/ns3:taskDetails/ns2:taskType"/>
                                        <br/>
                                        <b>Status: </b>
                                        <xsl:value-of select="ns3:taskData/ns3:taskDetails/ns2:status"/>
                                        <br/>
                                        <b>Created Time : </b>
                                        <xsl:value-of select="ns3:taskData/ns3:taskDetails/ns2:createdTime"/>
                                        <br/>
                                        <b>Last Modified Time : </b>
                                        <xsl:value-of select="ns3:taskData/ns3:taskDetails/ns2:lastModifiedTime"/>
                                        <br/>
                                    </xsl:if>
                                    <b>Description : </b>
                                    <xsl:value-of select="ns3:taskData/ns3:description"/>
                                    <br/>
                                    <b>Input : </b>
                                    <xsl:if test="count(ns3:taskData/ns3:taskInput) = 1">
                                        <xsl:for-each select="ns3:taskData/ns3:taskInput/ns3:documentReference">
                                            <xsl:call-template name="extractAttachement"/>
                                        </xsl:for-each>
                                    </xsl:if>
                                    <br/>
                                    <b>Output : </b>
                                    <xsl:if test="count(ns3:taskData/ns3:taskOutput) = 1">
                                        <xsl:for-each select="ns3:taskData/ns3:taskOutput/ns3:documentReference">
                                            <xsl:call-template name="extractAttachement"/>
                                        </xsl:for-each>
                                    </xsl:if>
                                    <br/>
                                </xsl:if>

                                <table width="99%" cellspacing="1" cellpadding="5" style="margin-left:10px;margin-right:10px;margin-bottom:10px;">
                                    <thead style="font-weight: bold;">
                                        <tr>
                                            <td style="text-align:center;background-color:#99ccee;" colspan="5">TaskEventHistory</td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td style="background-color:#99ccee;">id</td>
                                            <td style="background-color:#99ccee;">eventTime</td>
                                            <td style="background-color:#99ccee;">identifier</td>
                                            <td style="background-color:#99ccee;">eventType</td>
                                            <td style="background-color:#99ccee;">status</td>
                                        </tr>
                                    </thead>
                                    <tbody cellspacing="1">
                                        <xsl:if test="count(ns3:taskEventHistory) = 1">
                                            <xsl:for-each select="ns3:taskEventHistory/ns3:taskEvent">
                                                <tr>
                                                    <td style="background-color:#88bbdd;">
                                                        <xsl:value-of select="ns3:id"/>
                                                    </td>
                                                    <td style="background-color:#88bbdd;">
                                                        <xsl:value-of select="ns3:eventTime"/>
                                                    </td>
                                                    <td style="background-color:#88bbdd;">
                                                        <xsl:value-of select="ns3:identifier"/>
                                                    </td>
                                                    <td style="background-color:#88bbdd;">
                                                        <xsl:value-of select="ns3:eventType"/>
                                                    </td>
                                                    <td style="background-color:#88bbdd;">
                                                        <xsl:value-of select="ns3:status"/>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:if>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </xsl:for-each>
                </xsl:if>
                
                <xsl:call-template name="listDocumentHistory"/>
                
            </body>
        </html>
    </xsl:template>
    
    <xsl:template name="generalInformation">
        <table width="100%" cellspacing="1">
            <tr>
                <td style="width:200px;background-color:#aaaaee;">Instance ID</td>
                <td style="background-color:#aaccff;">
                    <xsl:value-of select="/ns3:XDW.WorkflowDocument/ns3:workflowInstanceID"/>
                </td>
            </tr>
            
            <tr>
                <td style="width:200px;background-color:#aaaaee;">Sequence Number</td>
                <td style="background-color:#aaccff;">
                    <xsl:value-of select="/ns3:XDW.WorkflowDocument/ns3:workflowDocumentSequenceNumber"/>
                </td>
            </tr>
            
            <tr>
                <td style="width:200px;background-color:#aaaaee;">Status</td>
                <td style="background-color:#aaccff;color : #0000aa; font-weight : bold;">
                    <xsl:value-of select="/ns3:XDW.WorkflowDocument/ns3:workflowStatus"/>
                </td>
            </tr>
            
            <tr>
                <td style="width:200px;background-color:#aaaaee;">WD Reference</td>
                <xsl:for-each select="/ns3:XDW.WorkflowDocument/ns3:workflowDefinitionReference">
                    <td style="background-color:#aaccff;">
                        <xsl:call-template name="getContent">
                            <xsl:with-param name="content" select="/ns3:XDW.WorkflowDocument/ns3:workflowDefinitionReference"/>
                        </xsl:call-template>
                    </td>
                </xsl:for-each>
            </tr>
        </table>
    </xsl:template>
    
    <xsl:template name="patient">
        <h2>Patient</h2>
        <div style="margin-left:10px;margin-right:10px;margin-bottom:10px;background-color:#aaddff;width:99%;">
            
           <table style="width:100%;" cellspacing="1">
               <tr><td width="10%" style="background-color:#99ccee;"><b>Patient: </b></td>
                   <td width="40%" style="background-color:#99ccee;">
                       <xsl:if test="count(/ns3:XDW.WorkflowDocument/ns3:patient) = 1">
                           <xsl:call-template name="getName">
                               <xsl:with-param name="name" select="/ns3:XDW.WorkflowDocument/ns3:patient/ns3:name"/>
                           </xsl:call-template>
                       </xsl:if></td>
                   <td width="25%" align="right" style="background-color:#99ccee;"><b>MRN: </b></td>
                   <td width="25%" style="background-color:#99ccee;"><xsl:if test="count(/ns3:XDW.WorkflowDocument/ns3:patient) = 1">
                       <xsl:if test="count(/ns3:XDW.WorkflowDocument/ns3:patient/ns3:id) = 1">
                           <xsl:value-of select="/ns3:XDW.WorkflowDocument/ns3:patient/ns3:id/@extension"/>
                       </xsl:if>
                   </xsl:if></td>		       
               </tr>
               
               <tr>
                   <td width="15%" style="background-color:#99ccee;"><b>Birthdate: </b></td>
                   <td width="40%" style="background-color:#99ccee;"><xsl:if test="count(/ns3:XDW.WorkflowDocument/ns3:patient) = 1">
                       <xsl:if test="count(/ns3:XDW.WorkflowDocument/ns3:patient/ns3:birthTime) = 1">
                           <xsl:call-template name="formatDate">
                               <xsl:with-param name="date" select="/ns3:XDW.WorkflowDocument/ns3:patient/ns3:birthTime/@value"/>
                           </xsl:call-template>
                       </xsl:if>
                   </xsl:if></td>
                   <td width="25%" align="right" style="background-color:#99ccee;"><b>Sex: </b></td>
                   <td width="25%" style="background-color:#99ccee;"><xsl:if test="count(/ns3:XDW.WorkflowDocument/ns3:patient) = 1">
                       <xsl:if test="count(/ns3:XDW.WorkflowDocument/ns3:patient/ns3:administrativeGenderCode) = 1">
                           <xsl:variable name="sex" select="/ns3:XDW.WorkflowDocument/ns3:patient/ns3:administrativeGenderCode/@code"/>
                           <xsl:choose>
                               <xsl:when test="$sex='M'">Male</xsl:when>
                               <xsl:when test="$sex='F'">Female</xsl:when>
                           </xsl:choose>
                       </xsl:if>
                   </xsl:if></td>		       
               </tr>
           </table>
           
           <xsl:if test="count(/ns3:XDW.WorkflowDocument/ns3:patient) = 0">
               <div style="background-color:#77aaff;"><b>The patient is not defined !</b></div>
           </xsl:if>
        </div>
    </xsl:template>
    
    <xsl:template name="listAuthors">
        <h2>List of Authors</h2>
        <div style="margin-left:10px;margin-right:10px;margin-bottom:10px;background-color:#ffffaa;width:99%">
            <table style="width:100%; background-color:#ffffaa;" cellspacing="1">
                <tbody>
                    <xsl:for-each select="/ns3:XDW.WorkflowDocument/ns3:author">
                        <xsl:call-template name="createAuthor"/>
                    </xsl:for-each>
                </tbody>
            </table>
        </div>
    </xsl:template>
    
    <xsl:template name="listDocumentHistory">
        <xsl:if test="count(/ns3:XDW.WorkflowDocument/ns3:workflowStatusHistory) = 1">
            <h2>Worflow Status History</h2>
            <xsl:if test="not (count(/ns3:XDW.WorkflowDocument/ns3:workflowStatusHistory/ns3:documentEvent) = 0)">
                <div style="margin-left:10px;margin-right:10px;margin-bottom:10px;width:99%">
                    <table width="100%" style="background-color:#ffffaa;" cellspacing="1" cellpadding="5">
                        <thead style="font-weight: bold;">
                            <tr>
                                <td style="background-color:#eeee99;">eventTime</td>
                                <td style="background-color:#eeee99;">eventType</td>
                                <td style="background-color:#eeee99;">taskEventIdentifier</td>
                                <td style="background-color:#eeee99;">author</td>
                                <td style="background-color:#eeee99;">previousStatus</td>
                                <td style="background-color:#eeee99;">actualStatus</td>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:for-each select="/ns3:XDW.WorkflowDocument/ns3:workflowStatusHistory/ns3:documentEvent">
                                <tr>
                                    <td style="background-color:#dddd88;">
                                        <xsl:value-of select="ns3:eventTime"/>
                                    </td>
                                    <td style="background-color:#dddd88;">
                                        <xsl:value-of select="ns3:eventType"/>
                                    </td>
                                    <td style="background-color:#dddd88;">
                                        <xsl:value-of select="ns3:taskEventIdentifier"/>
                                    </td>
                                    <td style="background-color:#dddd88;">
                                        <xsl:value-of select="ns3:author"/>
                                    </td>
                                    <td style="background-color:#dddd88;">
                                        <xsl:value-of select="ns3:previousStatus"/>
                                    </td>
                                    <td style="background-color:#dddd88;">
                                        <xsl:value-of select="ns3:actualStatus"/>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </tbody>
                        
                    </table>
                </div>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="extractAttachement">        
        <xsl:value-of select="ns2:name"/> :: <xsl:value-of select="ns2:identifier"/> 
    </xsl:template>
    
    <xsl:template name="reference">        
        <xsl:value-of select="@uid"/>
    </xsl:template>
    
    <xsl:template name="createAuthor">
        <xsl:if test="count(ns3:assignedPerson) = 1">
           <tr>
               
               <td width="12%" valign="center" style="background-color: #dddd88;"><b>Author </b></td>
               <td width="40%" style="background-color: #dddd88;">
                   <xsl:call-template name="getName">
                       <xsl:with-param name="name" select="ns3:assignedPerson/ns1:name"/>
                   </xsl:call-template>
               </td>
               <td width="25%" align="right" valign="center" style="background-color: #dddd88;">
                   <b>MRN: </b>
               </td>
               <td width="25%" style="background-color: #dddd88;">
                   <xsl:if test="count(ns3:assignedPerson/ns1:typeId) = 1">
                        <xsl:value-of select="ns3:assignedPerson/ns1:typeId/@extension"/>
                    </xsl:if>
               </td>		       
           </tr>
        </xsl:if>
        
        <xsl:if test="count(ns3:assignedAuthoringDevice) = 1">
            <tr>
                
                <td width="12%" valign="center" style="background-color: #dddd88;"><b>Authoring Device </b></td>
                <td width="25%" style="background-color: #dddd88;" valign="center">
                    <xsl:if test="count(ns3:assignedAuthoringDevice/ns1:typeId) = 1">
                        <xsl:value-of select="ns3:assignedAuthoringDevice/ns1:typeId/@extension"/>
                    </xsl:if>
                </td>
                <td width="25%" align="right" style="background-color: #dddd88;vertical-align:center;">
                    <b>Informations: </b>
                </td>
                <td width="38%" style="background-color: #dddd88;" valign="center">
                    <b>Code: </b><xsl:if test="count(ns3:assignedAuthoringDevice/ns1:code) = 1">
                        <xsl:value-of select="ns3:assignedAuthoringDevice/ns1:code/@code"/></xsl:if><br/>
                    <b>manufacturer Model Name: </b><xsl:if test="count(ns3:assignedAuthoringDevice/ns1:manufacturerModelName) = 1">
                        <xsl:value-of select="ns3:assignedAuthoringDevice/ns1:manufacturerModelName/@code"/></xsl:if><br/>
                    <b>software Name: </b><xsl:if test="count(ns3:assignedAuthoringDevice/ns1:softwareName) = 1">
                        <xsl:value-of select="ns3:assignedAuthoringDevice/ns1:softwareName/@code"/></xsl:if>
                </td>		       
            </tr>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="getContent">
        <xsl:param name="content"/>
        <xsl:value-of select="$content"/>
    </xsl:template>

    <!-- Get a Name  -->
    <xsl:template name="getName">
        <xsl:param name="name"/>
        <xsl:choose>
            <xsl:when test="$name/ns3:family">
                <xsl:value-of select="$name/ns3:given"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$name/ns3:family"/>
                <xsl:text> </xsl:text>
                <xsl:if test="$name/ns3:suffix">
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="$name/ns3:suffix"/>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$name"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--  Format Date 
    
      outputs a date in Month Day, Year form
      e.g., 19991207  ==> December 07, 1999
-->
    <xsl:template name="formatDate">
        <xsl:param name="date"/>
        <xsl:variable name="month" select="substring ($date, 5, 2)"/>
        <xsl:choose>
            <xsl:when test="$month='01'">
                <xsl:text>January </xsl:text>
            </xsl:when>
            <xsl:when test="$month='02'">
                <xsl:text>February </xsl:text>
            </xsl:when>
            <xsl:when test="$month='03'">
                <xsl:text>March </xsl:text>
            </xsl:when>
            <xsl:when test="$month='04'">
                <xsl:text>April </xsl:text>
            </xsl:when>
            <xsl:when test="$month='05'">
                <xsl:text>May </xsl:text>
            </xsl:when>
            <xsl:when test="$month='06'">
                <xsl:text>June </xsl:text>
            </xsl:when>
            <xsl:when test="$month='07'">
                <xsl:text>July </xsl:text>
            </xsl:when>
            <xsl:when test="$month='08'">
                <xsl:text>August </xsl:text>
            </xsl:when>
            <xsl:when test="$month='09'">
                <xsl:text>September </xsl:text>
            </xsl:when>
            <xsl:when test="$month='10'">
                <xsl:text>October </xsl:text>
            </xsl:when>
            <xsl:when test="$month='11'">
                <xsl:text>November </xsl:text>
            </xsl:when>
            <xsl:when test="$month='12'">
                <xsl:text>December </xsl:text>
            </xsl:when>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="substring ($date, 7, 1)=&quot;0&quot;">
                <xsl:value-of select="substring ($date, 8, 1)"/>
                <xsl:text>, </xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="substring ($date, 7, 2)"/>
                <xsl:text>, </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="substring ($date, 1, 4)"/>
    </xsl:template>

</xsl:stylesheet>